import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';
import { Actions } from 'react-native-router-flux';

import {
  Button,
  Snackbar,
 } from 'react-native-paper';

import {
  StyleSheet,
  Alert,
  Image,
  Text,
  View,
  Dimensions,
  ActivityIndicator
} from 'react-native';

import { theme, sleep } from '../App.react';

import logo from '../Imagens/logo-athena-br-black.png';

const { height, width } = Dimensions.get('window');;

type Props = {};
export default class Iniciar extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      load: true,
      visible: false,
    };

    this.refUsuarios = firebase.firestore().collection('usuarios');
    this.refCelulares = firebase.firestore().collection('celulares');

    this.uniqueId = DeviceInfo.getUniqueID();
  }

  componentDidMount = async () => {
    let isOpenIntroducao = false;
    const introducao = await this.refCelulares.where('uniqueId','==', this.uniqueId).limit(1).get();
    if(!introducao.docs[0]) {
      isOpenIntroducao = true;
      this.refCelulares.add({
        uniqueId: this.uniqueId,
        open: true,
      });
    }

    if(isOpenIntroducao) {
      Actions.introducao({});
      return;
    }

    await sleep(250);
    const user = Actions.user;

    if(!user) {
      await sleep(550);
      Actions.login({});
      return;
    }

    const usuarioDb = await this.refUsuarios.where('token', '==', user.uid).get();
    const usuario = usuarioDb.docs[0];

    if(user && user.emailVerified && usuario && usuario.data().lembrar){
      const fcmToken = await firebase.messaging().getToken();
      this.refUsuarios.doc(usuario.id).update({ fcmToken });
      Actions.usuario = usuario;
      Actions.grupos();
    } else {
      Actions.login({});
    }

    this.setState({ load: false });



    // setTimeout(async() => {
    //   if(this.state.user && this.state.user.emailVerified){
    //     const fcmToken = await firebase.messaging().getToken();
    //     const tabelaUsuario = await this.refUsuarios.where('token', '==', this.state.user.uid).get();
    //     console.warn(fcmToken);
    
    //     this.refUsuarios.doc(tabelaUsuario.docs[0].id).update({
    //       fcmToken,
    //     });

    //     Actions.grupos();
    //   } else if(this.state.user && !this.state.user.emailVerified){
    //     this.setState({ visible: true });
    //   }

    //   this.setState({ load: false });
    // }, 150);
  }

  // onPressLogin = async () => {
  //   const { email, senha } = this.state;

  //   if(!email && !senha){
  //     Alert.alert(
  //       'Ops!',
  //       'Preencha email e senha para sabermos quem é você!',
  //       [
  //         {text: 'OK', onPress: async () => {}},
  //       ],
  //       { cancelable: false }
  //     );
  //     return;
  //   }

  //   this.setState({ isLoad: true });
  //   const permitido = await this.entrar(email, senha);
    
  //   if(permitido && permitido.user.emailVerified){
  //     Actions.grupos();                  
  //   } else if (permitido && !permitido.user.emailVerified) {
  //     Alert.alert(
  //       'Ops!',
  //       'Voce ainda não verificou seu e-mail? Poxa vai lá, é para a sua segunraça!',
  //       [
  //         {text: 'OK', onPress: () => console.log('OK Pressed')},
  //       ],
  //       { cancelable: false }
  //     );
  //   }
  //   this.setState({ isLoad: false });
  // }

  // getMensagemErroEntrar = (code) => {
  //   switch (code) {
  //     case 'auth/invalid-email':
  //       return 'Email invalido!';
  //       break;
  //     case 'auth/user-disabled':
  //       return 'Usuario foi desativado!';
  //       break;
  //     case 'auth/user-not-found':
  //       return 'Usuario não encontrado!';
  //       break;
  //     case 'auth/wrong-password':
  //       return 'Senha invalido !';
  //       break;
      
  //     default:
  //       return 'Não foi possivel identificar o que aconteceu!\nTente novamente em alguns instantes!';
  //       break;
  //   }
  // }

  // entrar = async (usuario, senha) => {
  //   try {
  //     const user = await firebase.auth().signInAndRetrieveDataWithEmailAndPassword(usuario, senha);
  //     return user;
      
  //   } catch (error) {
  //     const msg = this.getMensagemErroEntrar(error.code);

  //     Alert.alert(
  //       'Ops!',
  //       msg,
  //       [
  //         {text: 'OK', onPress: () => console.log('OK Pressed')},
  //       ],
  //       { cancelable: false }
  //     )
  //     return false;
  //   }
  // }


  render = () => (
    <View style={styles.container} backgroundColor={theme.colors.primary}>        
      <Image source={logo} style={styles.logo}/>

      <View style={[styles.container, { backgroundColor: 'transparent' }]}>
        <ActivityIndicator size="large" color="white" />
        <Text style={{ color: "white", fontSize: 20, fontWeight: "900" }}>Aguarde...</Text>
      </View>

      <Button
          onPress={() => this.setState(state => ({ visible: !state.visible }))}
        >
          {this.state.visible ? 'Hide' : 'Show'}
        </Button>
        <Snackbar
          visible={this.state.visible}
          onDismiss={() => this.setState({ visible: false })}
          action={{
            label: 'Undo',
            onPress: () => {
              
            },
          }}
        >
          Hey there! I'm a Snackbar.
        </Snackbar>
          
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    flex: 1,
    width,
    resizeMode: 'center',
  },
});
