import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';

import {
  Button,
  Title,
  Snackbar,
  IconButton,
 } from 'react-native-paper';

import {
  StyleSheet,
  Alert,
  Image,
  Text,
  View,
  Dimensions,
  ActivityIndicator
} from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider';

import { theme, sleep } from '../App.react';

import logo from '../Imagens/logo-athena-br-black.png';
import imgSlide2 from '../Imagens/introducao/slide-2.jpg';
import imgSlide3 from '../Imagens/introducao/slide-3.png';

const { height, width } = Dimensions.get('window');

type Props = {};
export default class Introducao extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      load: true,
      visible: false,
      showRealApp: false,
    };
  
    this.slides = [
      {
        key: 'boas-vindas',
        title: 'OLÁ, SOU A CORUJA Athena.',
        text: 'Eu vou te mostrar um pouco mais do app!',
        image: logo,
        imageStyle: {
          width,
          height: 250,
        },
        backgroundColor: theme.colors.primary,
      },
      {
        key: 'grupos',
        title: 'GRUPOS',
        text: 'Com os grupos e subgrupos você pode se organizar melhor para gerenciar seus estudos ou até mesmo seus roles! 😎',
        image: imgSlide2,
        imageStyle: {
          width,
          height: 400,
        },
        backgroundColor: '#46badf',
      },
      {
        key: 'voce',
        title: 'TUDO PARA VOCÊ',
        text: 'A experiencia do usuario é tudo para nós! \n Nos desculpe pelas propagandas é só para poder deixar tudo na faixa! 🤑',
        image: imgSlide3,
        imageStyle: {
          width,
          height: 400,
        },
        backgroundColor: '#1976d2',
      }
    ];
  }

  componentDidMount = async () => {

  }

// <View style={styles.container} backgroundColor={theme.colors.primary}>        
      /* <Image source={logo} style={styles.logo}/>

      <View style={[styles.container, { backgroundColor: 'transparent' }]}>
        <ActivityIndicator size="large" color="white" />
        <Text style={{ color: "white", fontSize: 20, fontWeight: "900" }}>Aguarde...</Text>
      </View> */

  onDone = () => {
    this.setState({ showRealApp: true });
  }

  renderPrevButton = () => (
    <View style={{}}>
      <IconButton
        icon="chevron-left"
        color="white"
        size={28}
        // onPress={() => {

        // }}
      />
    </View>
  )

  renderNextButton = () => (
    <View style={{}}>
      <IconButton
        icon="chevron-right"
        color="white"
        size={28}
        // onPress={() => {

        // }}
      />
    </View>
  )

  renderDoneButton = () => (
    <View style={{}}>
      <IconButton
        icon="done"
        color="white"
        size={28}
        onPress={() => {
          Actions.login({});
        }}
      />
    </View>
  )

  renderSkipButton = () => (
    <View style={{}}>
      <IconButton
        icon="launch"
        color="white"
        size={28}
        onPress={() => {
          Actions.login({});
        }}
      />
    </View>
  )
  

  render = () => (
    <AppIntroSlider
      slides={this.slides}
      renderDoneButton={this.renderDoneButton}
      renderNextButton={this.renderNextButton}
      showPrevButton
      renderPrevButton={this.renderPrevButton}
      showSkipButton
      renderSkipButton={this.renderSkipButton}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 320,
    height: 320,
  },
  logo: {
    flex: 1,
    width,
    maxHeight: 300,
    resizeMode: 'center',
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
});
