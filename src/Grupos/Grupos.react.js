import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';
import Swipeable from 'react-native-swipeable';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';

import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, Image, Alert, Platform, Linking, Text, View, ActivityIndicator, Dimensions, TouchableOpacity } from 'react-native';

import { 
    Provider as PaperProvider,
    Appbar,
    Searchbar,
    Button,
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme, criarPropaganda, sleep } from '../App.react';
import { tokenAdMob } from '../constantes';

const advert = firebase.admob().rewarded('ca-app-pub-5152531393270719/8758938338');
const Banner = firebase.admob.Banner;
const AdRequest = firebase.admob.AdRequest;

const { width, height } = Dimensions.get('window');

type Props = {};

export default class Grupos extends Component<Props> {
    constructor(props) {
        super(props);

        this.actions = [ {
            text: 'Add grupo',
            icon: <Icon name="group-add" color="white" size={24} />,
            name: 'group',
            position: 1
          },
        //   {
        //     text: 'Add notificação',
        //     icon: <Icon name="notifications" color="white" size={24} />,
        //     name: 'notifications-active',
        //     position: 2
        //   }
        ];

        this.state = {
            pesquisa: '',
            isPesquisa: false,
            onRewarded: false,
            currentlyOpenSwipeable: null,
        };

        this.itemProps = {
            onOpen: (event, gestureState, swipeable) => {
                if (this.state.currentlyOpenSwipeable && this.state.currentlyOpenSwipeable !== swipeable) {
                    this.state.currentlyOpenSwipeable.recenter();
                }
        
                this.setState({currentlyOpenSwipeable: swipeable});
            },
            onClose: () => this.setState({currentlyOpenSwipeable: null})
        };

        this.storage = firebase.storage();

        this.refGrupos = firebase.firestore().collection('grupos');
        this.refSubgrupos = firebase.firestore().collection('sub-grupos');
        this.refMensagens = firebase.firestore().collection('mensagens');

        this.refUsuarios = firebase.firestore().collection('usuarios');
        this.token = this.refUsuarios.where('token', '==', Actions.user.uid);
    }

    componentWillReceiveProps = (nprops) => {
        const { id } = nprops;
        if(id !== null) {
            this.listView.refresh();
        }
    }
        
    deletarGrupo = (grupo) => {
        Alert.alert(
            'Ops!',
            'Você deseja mesmo excluir o grupo e os subgrupos?\nEssa ação não pode ser desfeita!',
            [
              {text: 'Cancelar', onPress: () => console.log('cancelar Pressed')},
              {text: 'Excluir', onPress: async () => {
                await this.refGrupos.doc(grupo.id).delete()
                .then(async res => {
                    if(grupo.data().photoURL){
                        const img = this.storage.refFromURL(grupo.data().photoURL);
                        if(img){
                            img.delete();
                        }    
                    }

                    const subgrupos = await this.refSubgrupos.where('idGrupo', '==', grupo.id).get();
                    subgrupos.docs.forEach(async subgrupo => {
                        await this.refSubgrupos.doc(subgrupo.id).delete();
                    });

                    const mensagens = await this.refMensagens.where('idGrupo', '==', grupo.id).get();
                    mensagens.docs.forEach(async mensagem => {
                        await this.refMensagens.doc(mensagem.id).delete();
                    });

                    this.itemProps.onOpen();
                    this.listView.refresh();
                })
                .catch(e => {
                    console.warn(e.message);
                })                
              }},
            ],
            { cancelable: false }
        );
    }

    onFetch = async (page = 1, startFetch, abortFetch) => {
        try {
            let meusGrupos = [];
            let pageLimit = 24
            const skip = (page - 1) * pageLimit
    
            const tokenUsuario = await this.token.get();
            const grupos = await this.refGrupos.get();
 
            if(grupos.docs.length){
                meusGrupos = grupos.docs
                    .filter((grupo) => grupo.data().nome && grupo.data().nome.indexOf(this.state.pesquisa) > -1)
                    .filter(
                    (grupo) => grupo.data().usuarios.find(
                        user => user == Actions.user.uid || user == tokenUsuario.docs[0].id
                    )
                );
            }

            startFetch(meusGrupos, pageLimit);
        } catch (err) {
            abortFetch();
            console.warn(err.message);
        }
    }

    renderMenu = () => (
        <RNPopover visible={this.state.visible} reference={this.ref}>
          <RNPopover.Menu label={"Ações"}>
        
              <RNPopover.Menu
                label="Editar"
                icon={<VecIcon name="edit" color="black" size={24} family={"MaterialIcons"} />}
              />
              <RNPopover.Menu
                label="Excluir"
                icon={<VecIcon name="delete" color="black" size={24} family={"MaterialIcons"} />}
              />
        
          </RNPopover.Menu>
        </RNPopover>
    )

    renderBtnEdicao = (grupo) => (
        <View style={[styles.btnEditarItemLista, { backgroundColor: theme.colors.accent }]}>
            <TouchableOpacity
                style={styles.swipeableTouch}
                onPress={() => {
                    this.itemProps.onOpen();
                    Actions.editGrupos({
                        titulo: 'Editar grupo',
                        grupo: grupo.data(),
                        idGrupo: grupo.id,
                        subgrupo: false,
                        usuarios: grupo.data().usuarios.map(usuario => usuario.id),
                        chips: Object.values(grupo.data().usuarios),
                    });
                }}
            >
                <Icon name="edit" color="white" size={35} />
                <Text style={{ color: 'white' }} >Editar</Text>
            </TouchableOpacity>
        </View>
    )

    renderBtnExclusao = (grupo) => (
        <View style={styles.btnExcluirItemLista}>
            <TouchableOpacity
                style={styles.swipeableTouch}
                onPress={() => this.deletarGrupo(grupo)}
            >
                <Icon name="delete-forever" color="white" size={35} />
                <Text style={{ color: 'white' }} >Excluir</Text>
            </TouchableOpacity>
        </View>
    )

    renderItem = (grupo, sectionID, rowID) => (
        <Swipeable
            {...this.itemProps}
            rightButtons={[
                this.renderBtnEdicao(grupo),
                this.renderBtnExclusao(grupo),
            ]}
            onRightButtonsOpenRelease={this.itemProps.onOpen}
            onRightButtonsCloseRelease={this.itemProps.onClose}
       >
            <ListItem
                key={`grupo-${grupo.id}`}
                centerElement={{
                    primaryText: grupo.data().nome,
                    secondaryText: grupo.data().descricao || 'Nenhuma descrição definida',
                    tertiaryText: null
                }}
                leftElement={
                    <Avatar 
                        size={45}
                        text={grupo.data().photoURL ? null : grupo.data().nome.substring(0,1)}
                        image={
                            grupo.data().photoURL ?
                            <Image
                                style={styles.avatarContent}
                                source={{ uri: grupo.data().photoURL }}
                            />
                            : null 
                        }
                        style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 20 } }}
                    />
                }
                rightElement={
                    grupo.data().atividades ? <Badge
                        key={`bad-grupo-${grupo.id}`}
                        size={24}
                        text={`${grupo.data().atividades}`}
                        style={{ content: { color: 'white' }, container: { position: 'relative', top: 0, right: 0 } }}
                    />
                    : null
                }
                onPress={() => Actions.subGrupos({grupo})}
            />
        </Swipeable>
    )

    renderEmptyView = () => (
        <View style={[styles.container, { height: height / 1.2 }]}>
            <Icon name={'search'} color={theme.colors.primary} size={65} />
            <Text>Nada encontrado!</Text>
        </View>
    )

    loadOpen = () => (
        <View style={[styles.container, { height: height / 1.2 }]}>
            <ActivityIndicator size="large" color={theme.colors.primary} />
            <Text>Aguarde...</Text>
        </View>
    )

    renderAppbar = () => (
        <Appbar.Header>
            <Appbar.Action icon="menu" onPress={() => Actions.drawerOpen()} />
            <Appbar.Content
                title="Athena"
                subtitle={null}
            />
            <Appbar.Action
                icon="search"
                onPress={() => {
                    this.setState({ isPesquisa: true });
                    setTimeout(() => {
                        this.search.focus();
                    }, 250);
                }}
            />
        </Appbar.Header>
    ) 

    renderBusca = () => (
        <View style={{ padding: 5, backgroundColor: theme.colors.primary }} >
            <Searchbar
                ref={ref => this.search = ref}
                icon="arrow-back"
                placeholder="Pesquisa"
                onChangeText={pesquisa => { 
                    this.setState({ pesquisa });
                    this.listView.refresh();
                }}
                value={this.state.pesquisa}
                onIconPress={() => this.setState({ isPesquisa: false })}
            />
        </View>
    )
      
    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">

            {this.state.isPesquisa ? this.renderBusca() : this.renderAppbar()}

            <View flex={1}>
                <UltimateListView
                    flex={1}
                    ref={ref => this.listView = ref}
                    key={'ultimatelistacontato'}
                    onFetch={this.onFetch}
                    keyExtractor={(item) => item.id} 
                    refreshableMode="basic" // basic or advanced
                    item={this.renderItem} // this takes three params (item, index, separator)       
                    displayDate
                    allLoadedText="Acabou 😉"
                    arrowImageStyle={{ width: 20, height: 20, resizeMode: 'contain' }}

                    spinnerColor={theme.colors.primary}
                    waitingSpinnerSize="small"
                    waitingSpinnerText="Aguarde ..."

                    paginationFetchingView={this.loadOpen}
                    emptyView={this.renderEmptyView}
                />

                <Banner
                    unitId={
                        Platform.OS === 'ios'
                        ? "ca-app-pub-5152531393270719/5506400098"
                        : "ca-app-pub-5152531393270719/4393932318"
                    }
                    size={"SMART_BANNER"}
                    onAdLoaded={() => {
                        // console.warn('Advert loaded');
                    }}
                    onAdFailedToLoad={(e) => {
                        //     console.warn(e);

                        // setInterval(() => {
                        //     setTimeout(() => {
                        //         if(this.smartBannerExample){
                        //             this.smartBannerExample.loadBanner();
                        //         }
                        //     }, 100);              
                        // }, 100);

                    }}
                />

                <FloatingAction
                    ref={(ref) => { this.floatingAction = ref; }}
                    actions={this.actions}
                    onPressItem={
                        (name) => {
                            try {
                                switch (name) {
                                    case 'group':
                                        criarPropaganda(
                                            () => Actions.addGrupos({ titulo: 'Selecionar participantes' })
                                        );
                                        break;
                                    default:                                        
                                        // this.criarPropaganda(
                                        //     () => Actions.addGrupos({ titulo: 'Selecionar participantes' })
                                        // );
                                        break;
                                }
                            } catch (error) {
                                console.warn(error.message);
                            }

                        }
                    }
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
    avatarContent: {
        resizeMode:"cover",
        height: 45,
        width: 45,
        borderRadius: 22.5,
    },
    swipeableTouch: {
        padding: 10,
        marginLeft: 10,
        alignContent: 'center',
        alignItems: 'center',
    },
    btnEditarItemLista: {
        alignContent: 'flex-start',
        alignItems: 'flex-start',
    },
    btnExcluirItemLista: {
        backgroundColor: COLOR.red500,
        alignContent: 'flex-start',
        alignItems: 'flex-start',
    },
});
