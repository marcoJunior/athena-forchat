import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
import Stepper from 'react-native-js-stepper';
import { Actions } from 'react-native-router-flux';
import { StyleSheet, Image, Text, View, Dimensions, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';

import { 
    Provider as PaperProvider,
    Button,
    TextInput,
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';

const { width, height } = Dimensions.get('window');

type Props = {
    titulo: string
};

export default class FormGrupo extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
        };
    }

    

    selecionarFotoDePerfil = () => {
        const options = {
          title: 'Select Avatar',
        //   customButtons: [
        //     {name: 'fb', title: 'Choose Photo from Facebook'},
        //   ],
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
        
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        
            this.setState({
              avatarSource: source
            });
          }
        });
    }

    renderAvatar = (source) => (
        <View
          flex={1}
          margin={8}
        >
          <Avatar 
              xlarge
              rounded
              size={120}
              text={source ? null : 'G'}
              image={
                source ?
                <Image
                    style={styles.avatarContent}
                    source={source}
                />
                : null 
              }
              style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 35 } }}
          />
            <Button
              primary
              // style={styles.botaoLogar}
              onPress={this.selecionarFotoDePerfil}
            >
              <Text style={{ fontSize: 18 }}>editar</Text>
            </Button>
        </View>
    )

    render = () => (
        <View flex={1} backgroundColor="white">
            {/* <ScrollView> */}
                <View style={styles.container}>
                    {this.renderAvatar(this.state.avatarSource)}

                    <KeyboardAvoidingView
                        style={styles.container}
                        behavior={Platform.OS === 'ios' ? 'padding' : null}
                        keyboardVerticalOffset={Platform.OS === 'ios' ? 128 : 0}
                    >
                        <TextInput
                            label='Nome'
                            style={styles.input}
                            value={this.state.nome}
                            onChangeText={nome => this.setState({ nome })}
                        />
                        <TextInput
                            label='Descrição'
                            style={styles.input}
                            value={this.state.descricao}
                            onChangeText={descricao => this.setState({ descricao })}
                        />
                        
                    </KeyboardAvoidingView>
                </View>
            {/* </ScrollView> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      avatarContent: {
        resizeMode:"cover",
        height: 122,
        width: 122,
        borderRadius: 60,
      },
      logo: {
        flex: 1,
        resizeMode: 'contain',
      },
      input: {
        marginHorizontal: 10,
        width: width - 30,
      },
      containerInputs: {
        marginVertical: 5,
        borderRadius: 5,
      },
      botaoLogar: {
        height: 42,
        width: width - 10,
      },
});
