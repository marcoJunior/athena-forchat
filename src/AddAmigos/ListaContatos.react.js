import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import difference from 'lodash/difference';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, Alert, Image, Linking, Text, View, ActivityIndicator, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Searchbar,
    Button,
    Checkbox,
    Appbar
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';
const { width, height } = Dimensions.get('window');

type Props = {
    pesquisa: string,
};

export default class ListaContatos extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            usuario: {},
            usuarios: {},
        };

        this.refUsuarios = firebase.firestore().collection('usuarios');
    }

    componentDidMount = async() => {
        await this.findUserAndAmigos();
        this.listView.refresh();
    }

    componentWillReceiveProps = (nprops) => {
        const { pesquisa } = nprops;
        if(pesquisa !== null) {
            this.setState({ pesquisa });
            this.listView.refresh();
        }
    } 

    findUserAndAmigos = async() => {
        if(this.state.pesquisa.trim() === ''){
            return [];
        }
        
        const usuario = await this.refUsuarios
                .where('token','==', this.state.user.uid)
                .get();

        const findMeusAmigos = usuario.docs[0].data().amigos || [];

        const usuarios = await this.refUsuarios.get();
        this.setState({ usuarios, usuario, findMeusAmigos });

        return usuarios.docs
            .filter((user) => user.data().token !== Actions.user.uid)
            .filter((user) => user.data().nome.indexOf(this.state.pesquisa) > -1)
            .filter(item => {
                const { token } = item.data();
                return !findMeusAmigos.find(usr => usr === token );
            });
    }

    onFetch = async (page = 1, startFetch, abortFetch) => {
        try {
            const amigos = await this.findUserAndAmigos();
    
            startFetch(amigos, 10);
        } catch (err) {
            abortFetch();
            console.log(err);
        }
      }

    renderItem = (usuario, item2) => {
      return (
        <ListItem
            key={`grupo-${usuario.id}`}
            centerElement={{
                primaryText: usuario.data().nome,
                secondaryText: usuario.data().descricao || 'Informacoes complementares',
                tertiaryText: null
            }}
            leftElement={
              <Avatar 
                xlarge
                rounded
                size={45}
                text={usuario.data().photoURL ? null : usuario.data().nome.substring(0,1)}
                image={
                  usuario.data().photoURL ?
                  <Image
                      style={styles.avatarContent}
                      source={{ uri: usuario.data().photoURL }}
                  />
                  : null 
                }
                style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 20 } }}
              />
            }
            onPress={async () => {
                const eu = await this.refUsuarios.where('token','==', this.state.user.uid).get();
                let amigos = Object.assign([], eu.docs[0].data().amigos);
                amigos.push(usuario.data().token);

                Alert.alert(
                    'Add amigo!',
                    `Deseja mesmo adiconar ${usuario.data().nome} como seu amigo ?`,
                    [
                      {text: 'Cancelar', onPress: () => console.log('cancelar Pressed')},
                      {text: 'Confirmar', onPress: async () => {
                            this.refUsuarios.doc(eu.docs[0].id).update({
                                amigos
                            });
                            Actions.pop();

                            setTimeout(() => {
                                Actions.refresh({});
                            }, 10);
                      }},
                    ],
                    { cancelable: false }
                );
            }}
        />
      )
    }

    renderEmptyView = () => (
        <View style={[styles.container, { height: height / 1.4 }]}>
            <Icon name={'search'} color={theme.colors.primary} size={65} />
            <Text>{this.state.pesquisa.trim() === '' ? 'Diga o nome do seu amigo!' : 'Nada encontrado!'}</Text>
            
        </View>
    )

    loadOpen = () => (
        <View style={[styles.container, { height: height / 1.4 }]}>
            <ActivityIndicator size="large" color={theme.colors.primary} />
            <Text>Aguarde...</Text>
        </View>
    )

    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
            <View flex={1}>
                <UltimateListView
                    pagination={false}
                    autoPagination={false}
                    ref={ref => this.listView = ref}
                    key={'ultimatelistacontato'}
                    onFetch={this.onFetch}
                    keyExtractor={(item) => item.id} 
                    refreshableMode="basic"
                    item={this.renderItem}
                    displayDate
                    allLoadedText="Acabou 😉"
                    arrowImageStyle={{ width: 20, height: 20, resizeMode: 'contain' }}

                    paginationFetchingView={this.loadOpen}
                    emptyView={this.renderEmptyView}
                />
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    avatarContent: {
      resizeMode:"cover",
      height: 48,
      width: 48,
      borderRadius: 24,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
});
