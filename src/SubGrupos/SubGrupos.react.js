import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import { GiftedChat, Send, Actions as ChatActions, Bubble, SystemMessage, Message } from 'react-native-gifted-chat'
import { FloatingAction } from 'react-native-floating-action';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, TouchableOpacity, ActivityIndicator, Image, Linking, Text, View, Dimensions } from 'react-native';

import Snackbar from 'react-native-snackbar';

import { 
    Provider as PaperProvider,
    Searchbar,
    Button,
    Appbar,
    IconButton,
    FAB,
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme, criarPropaganda } from '../App.react';
import AthenaMessage from './AthenaMessage.react';

import { Icon as IconVector } from 'react-native-vector-icons'

const { width, height } = Dimensions.get('window');

type Props = {
    idUsuario: string,
    idGrupo: string,
    idSubGrupo?: string,
    nomeGrupo?: string,
};

export default class SubGrupos extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            load: false,
            received: false,
            isPergunta: false,
            openMenu: false,
            messages: [],
        };

        this.refMensagens = firebase.firestore().collection('mensagens');
        this.refUsuarios = firebase.firestore().collection('usuarios');

        this.refGrupo = firebase.firestore().collection('grupos');
        this.refSubGrupo = firebase.firestore().collection('sub-grupos');
        
        if(this.props.idSubGrupo){
            firebase.messaging().subscribeToTopic(`athena-${this.props.idSubGrupo}`);
        }

    }

    componentWillReceiveProps = async (nprops) => {
        const { notification } = nprops;
        // console.warn('aqui', notification);
        if(notification) {
            this.setState({ received: true })
            await this.carregarMensagens(false);
        }
    } 
    
    componentWillMount = async () => {
        await this.carregarMensagens();
    }

    renderLoading = () => (
        <View style={styles.container}>
            <ActivityIndicator size="large" color={theme.colors.primary} />
            <Text>Aguarde...</Text>
        </View>
    )

    carregarMensagens = async (isLoad = true) => {
        this.setState({ load: isLoad });

        const usuarios = await this.refUsuarios.get();
        const whereMsg = this.refMensagens
            .where('idGrupo', '==', this.props.idGrupo)
            .where('idSubGrupo', '==', this.props.idSubGrupo);

        const mensagens = await whereMsg.get();
        const mensagensOrder = mensagens.docs
            .sort(function(x, y){
                return x.data().createdAt - y.data().createdAt;
            }).reverse();
        
        const messages = [];
        mensagensOrder.forEach((item, index) => {
            const usuario = usuarios.docs.filter((user) => user.data().token == item.data().idUsuario);
            messages.push({
                _id: index,
                idMensagem: item.id,
                text: item.data().mensagem,
                createdAt: item.data().createdAt,
                sent: item.data().sent,
                received: item.data().received,
                isPergunta: item.data().isPergunta || false,
                user: {
                    _id: usuario[0].data().token,
                    name: usuario[0].data().nome,
                    avatar: usuario[0].data().photoURL,
                },
            });

        });

        setTimeout(() => {
            this.setState({ messages, load: false });
            // console.warn('usuarios id ',this.state.user.uid);
        }, 700);
    }

    onSend = async (messages = []) => {
        const msg = {
            idGrupo: this.props.idGrupo,
            idSubGrupo: this.props.idSubGrupo || null,
            idUsuario: this.state.user.uid,
            mensagem: messages[0].text,
            createdAt: new Date(),
            sent: true,
            isPergunta: this.state.isPergunta,
            respostas: [],
        };
        this.refMensagens.add(msg);

        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
            isPergunta: false,
        }));

        let usuariosAgrupados;
        if(this.props.idSubGrupo){
            usuariosAgrupados = await this.refSubGrupo.doc(this.props.idSubGrupo).get();
        } else {
            usuariosAgrupados = await this.refGrupo.doc(this.props.idGrupo).get();
        }

        const findUsers = usuariosAgrupados.data().usuarios || [];
        const usuarios = await this.refUsuarios.get();

        const meusAmigosSelecionados = usuarios.docs
            .filter((user) => user.data().token !== this.state.user.uid)
            .filter(item => {
                const { token } = item.data();
                return !findUsers.find(usr => usr === token );
            });

        meusAmigosSelecionados.map(participante => {
            // console.warn(participante.data().fcmToken);
            const message = new firebase.messaging.RemoteMessage()
            .setMessageId(participante.data().fcmToken)
            .setTo('athena-forchat@gcm.googleapis.com')
            .setData({
                message: messages[0].text,
            });
            
            firebase.messaging().sendMessage(message);
        });

        await this.carregarMensagens(false);
    }

    openMessagePergunta = (mensagem) => {
        if (!mensagem.isPergunta) {
            return;
        }

        console.warn(mensagem);
        Actions.pergunta({ mensagem, nomeGrupo: this.props.nomeGrupo });
    }

    renderMessage = (props) => (<Message {...props} />)

    renderTick = (name) => (<Icon name={name} color={"white"} size={14} style={{ marginTop: 10 }} />)

    renderTicks = (currentMessage) => {
        if (currentMessage.user._id !== this.state.user.uid) {
            return null;
        }
        if (currentMessage.sent || currentMessage.received) {
            return (
                <View style={styles.tickView}> 
                    {this.renderTick(currentMessage.sent && currentMessage.received ? 'done-all' : 'done')}
                </View>
            )
        }
    }

    renderBubble = (props) => (
        <Bubble
            {...props}
            wrapperStyle={{
                right: {
                    backgroundColor: theme.colors.accent,
                }
            }}
            renderTicks={this.renderTicks}
        />
    )

    renderCustomView = (props) => {
        const { currentMessage }  = props;
        const isUsuario = currentMessage.user._id === Actions.user.uid;

        if(isUsuario && !currentMessage.isPergunta){
            return;
        } else if(isUsuario && currentMessage.isPergunta){
            return (
                <TouchableOpacity style={styles.itemQuest} onPress={() => this.openMessagePergunta(currentMessage)}>
                    <Icon name="help" color="white" size={18} />
                    <Text style={{ color: 'white' }}> Você</Text>
                </TouchableOpacity>
            );    
        }

        return (
            <TouchableOpacity onPress={() => this.openMessagePergunta(currentMessage)} >
                {
                    currentMessage.isPergunta
                    ?
                    <View style={styles.itemQuest}>
                        <View flex={5}>
                            <Text>{currentMessage.user.name}</Text>
                        </View>
                        <View flex={1}>
                            <Icon name="help" color={theme.colors.accent} size={18} />
                        </View>
                    </View>
                    :
                    <View style={styles.itemQuest}>
                        <Text>{currentMessage.user.name}</Text>
                    </View>
                }
            </TouchableOpacity>
        );
    }


    renderSend = (props) => {
        return (
            <Send
                {...props}
            >
                <View style={styles.btnSend}>
                    <Icon name="send" color={theme.colors.accent} size={24} />
                </View>
            </Send>
        );
    }

    renderCustomActions = (props) => (
        <IconButton
            icon="add"
            size={24}
            color={theme.colors.accent}
            onPress={() => {
                this.setState({ openMenu: true });
            }}
        />
    )

    renderCustomActions2 = (props) => (
        <ChatActions
            {...props}
            icon={() => <Icon name="add" color={theme.colors.accent} size={24} />}
            options={{
                'Add Subgrupo': (props) => {
                    criarPropaganda(() => {
                        Actions.addGrupos({
                            titulo: 'Add Subgrupo',
                            subgrupo: true,
                            idGrupo: this.props.idGrupo,
                        });
                    });
                    return;
                },
                'Add Pergunta': (props) => {
                    criarPropaganda(() => {
                        const messages = {
                            text: 'Sua proxima mensagem sera anexada como pergunta!',
                            createdAt: new Date(),
                            system: true,
                        };

                        this.setState(previousState => ({
                            messages: GiftedChat.append(previousState.messages, messages),
                            isPergunta: true,
                        }));
                    });
                    return;
                },
                'Cancel': () => {},
            }}
        />
    );

    renderMenuBottom = () => (
        <Appbar style={styles.bottom}>
            <Appbar.Action
                icon="group-add"
                onPress={() => {
                    this.setState({ openMenu: false });

                    criarPropaganda(() => {
                        Actions.addGrupos({
                            titulo: 'Add Subgrupo',
                            subgrupo: true,
                            idGrupo: this.props.idGrupo,
                        });
                    });
                }}
            />
            <Appbar.Action
                icon="question-answer"
                onPress={() => {
                    this.setState({ openMenu: false });

                    Snackbar.show({
                        title: 'Ops... Logo menos esse recurso estára disponivel!',
                        duration: Snackbar.LENGTH_LONG,
                      });
    

                    // criarPropaganda(() => {
                    //     const messages = {
                    //         text: 'Sua proxima mensagem sera anexada como pergunta!',
                    //         createdAt: new Date(),
                    //         system: true,
                    //     };

                    //     this.setState(previousState => ({
                    //         messages: GiftedChat.append(previousState.messages, messages),
                    //         isPergunta: true,
                    //     }));
                    // });
                }}
            />
             <Appbar.Action
                icon="close"
                onPress={() => {
                    this.setState({ openMenu: false });
                }}
            />
        </Appbar>
    )

    render = () => (
        <View key={'chat-list'} flex={1} backgroundColor="#FFFFFF">
            <View flex={1}>
                {
                    this.state.load ?
                        this.renderLoading()
                        :
                        <GiftedChat
                            alwaysShowSend={true}
                            placeholder=""
                            // listViewProps={{
                            //     backgroundColor: 'black',
                            // }}
                            // lightboxProps={{
                            //     backgroundColor: 'black',
                            // }}
                            textInputProps={{
                                // height: 35,
                                paddingHorizontal: 10,
                                borderRadius: 15,
                                // backgroundColor: 'whitesmoke',
                            }}
                            renderSend={this.renderSend}
                            messages={this.state.messages}
                            onSend={messages => this.onSend(messages)}
                            user={{ 
                                _id: this.state.user.uid,
                                name: this.state.user.displayName,
                                avatar: this.state.user.photoUR,
                            }}

                            renderActions={this.renderCustomActions}
                            renderMessage={this.renderMessage}
                            renderBubble={this.renderBubble}
                            renderCustomView={this.renderCustomView}
                        />
                    }
            </View>
            { this.state.openMenu ? this.renderMenuBottom() : null}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
    btnSend: {
        borderRadius: 20,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    tickView: {
        flexDirection: 'row',
        marginRight: 10,
        marginTop: -10,
    },
    tick: {
        fontSize: 10,
        backgroundColor: 'transparent',
        color: 'white',
    },
    itemQuest: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginHorizontal: 8,
    },
    bottom: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
    },
});
