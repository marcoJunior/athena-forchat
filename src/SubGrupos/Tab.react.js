import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { FloatingAction } from 'react-native-floating-action';
import { View, StyleSheet, Dimensions } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import RNPopover from 'react-native-popover-menu';

import { Icon as VecIcon } from 'react-native-vector-icons/MaterialIcons';

import {
  DefaultTheme,
  Appbar,
  TextInput,
  Button,
  Switch,
} from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';

import { Actions } from 'react-native-router-flux';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import SubGrupos from './SubGrupos.react';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3b74c1',
    accent: '#92dbf6',
  },
};

type Props = {
  grupo: any
};

export default class Tab extends Component<Props> {
  constructor(props) {
    super(props);

    // this.actions = [{
    //   text: 'Add Subgrupo',
    //   icon: <Icon name="group-add" color="white" size={24} />,
    //   name: 'group',
    //   position: 1
    // }];

    this.refSubGrupos = firebase.firestore().collection('sub-grupos').where('idGrupo', '==', this.props.grupo.id);

    this.state = {
      user: firebase.auth().currentUser,
      grupo: this.props.grupo,
      visible: false,
      index: 0,
      routes: [{ key: this.props.grupo.data().id, title: this.props.grupo.data().nome }],
    };

    firebase.messaging().subscribeToTopic(`athena-${this.props.grupo.id}`);
    
  }

  componentWillReceiveProps = async (nprops) => {
    const { notification } = nprops;
    this.setState({ notification });
  } 

  componentDidMount = async() => {
    const subgrupos = await this.refSubGrupos.get();

    subgrupos.docs.reverse().forEach((item) => {
      this.state.routes.push({ key: item.id, title: item.data().nome });
    });

    this.setState({ routes: this.state.routes });

    this.smartBannerExample.loadBanner();
  }

  show = (ref) => {
    this.ref = ref

    this.setState({
      visible: true
    })
  }

  renderMenu = () => (
    <RNPopover visible={this.state.visible} reference={this.ref}>
      <RNPopover.Menu label={"Editar Subgrupo"}>
        {this.state.routes.map(route => 
          <RNPopover.Menu
            label={route.title}
            icon={<VecIcon name="edit" color="black" size={24} family={"MaterialIcons"} />}
          />
        )}

      </RNPopover.Menu>
      {/* <RNPopover.Menu >
        <RNPopover.Menu label={"Share"} icon={<VecIcon name="group-add" color="black" size={24} family={"MaterialIcons"} />} />
      </RNPopover.Menu> */}
    </RNPopover>
  )

  renderTabBar = (props) => (
    <TabBar
      {...props}
      style={{ backgroundColor: theme.colors.primary }}
      indicatorStyle={{ backgroundColor: theme.colors.accent }}
    />
  )

  renderScene = ({ route }) => (
    <SubGrupos
      key={route.key}
      flex={1}
      idUsuario={this.state.user.uid}
      idGrupo={this.state.grupo.id}
      idSubGrupo={route.key}
      nomeGrupo={route.title}
      notification={this.state.notification}
      />
  )

  render = () => (
    <View flex={1}>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => Actions.pop()}
        />
        <Appbar.Content
            title={this.state.grupo.data().nome || 'Grupo'}
            subtitle={this.state.grupo.data().descricao || null}
        />
        
        <Appbar.Action
          ref={ref => this.menu = ref}
          icon="more-vert"
          onPress={() => this.show(this.menu)}
        />
      </Appbar.Header>
        {this.renderMenu()}

      <TabView
        useNativeDriver
        navigationState={this.state}
        renderTabBar={this.renderTabBar}
        renderScene={this.renderScene}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
      />

      {/* <FloatingAction
        ref={(ref) => { this.floatingAction = ref; }}
        position="center"
        actions={this.actions}
        onPressItem={
            (name) => {
                switch (name) {
                    case 'group':
                        Actions.addGrupos({ titulo: 'Add Subgrupo', subgrupos: true });
                        break;
                    default:
                        console.log(`selected button: ${name}`);
                        break;
                }

            }
        }
      /> */}
    </View>
  );
}