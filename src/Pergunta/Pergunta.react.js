import React, { Component } from 'react';
import moment from 'moment';
import 'moment/locale/pt-br';
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, Image, Linking, Text, View, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Appbar,
    TextInput,
    Button, Card, Title, Paragraph,
    Toolbar, ToolbarBackAction, ToolbarContent, ToolbarAction
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';

const { width, height } = Dimensions.get('window');

type Props = {
  mensagem: any,
  nomeGrupo: string,
};

export default class Pergunta extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
          index: 0,
          routes: [
            { key: 0, title: 'Pergunta' },
            { key: 1, title: 'Respostas' }
          ],
        };
    }

    responder = () => {

    }

    renderAppbar = () => (
      <Appbar.Header>
          <Appbar.BackAction
              onPress={() => Actions.pop()}
          />

          <Appbar.Content
              title="Pergunta"
              subtitle={null}
          />
      </Appbar.Header>
    )

    renderTabBar = (props) => (
      <TabBar
        {...props}
        style={{ backgroundColor: theme.colors.primary }}
        indicatorStyle={{ backgroundColor: theme.colors.accent }}
      />
    )

    renderScene = ({ route }) => {
      if(route.key === 0) {
        return this.renderCardPergunta();
      }
      return <View />
    }

    renderUser = (mensagem) => (
      <ListItem
          centerElement={{
              primaryText: mensagem.user.name,
              secondaryText: moment(mensagem.createdAt).calendar() + ' em ' + this.props.nomeGrupo,
              tertiaryText: null,
          }}
          leftElement={
            <Avatar 
              xlarge
              rounded
              size={45}
              text={mensagem.user.avatar ? null : mensagem.user.name.substring(0,1)}
              image={
                mensagem.user.avatar ?
                <Image
                    style={styles.avatarContent}
                    source={{ uri: mensagem.user.avatar }}
                />
                : null 
              }
              style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 20 } }}
            />
          }
      />
    )

    renderCardPergunta = () => (
      <View style={styles.container}>
        <Card>
          {/* <Card.Cover source={{ uri: 'https://picsum.photos/700' }} /> */}

          <Card.Content>
            {this.renderUser(this.props.mensagem)}


            <Paragraph style={{ fontSize: 18, marginHorizontal: 4, marginBottom: 10 }}>
              {this.props.mensagem.text}
            </Paragraph>

            <TextInput
              label='Responder'
              value={this.state.resposta}
              onChangeText={resposta => this.setState({ resposta })}
            />
          </Card.Content>

          <Card.Actions>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Button
                onPress={() => this.responder()}
              >
                Responder
              </Button>
            </View>
          </Card.Actions>
        </Card>
      </View>
    )

    render = () => (
        <View flex={1}>
          {this.renderAppbar()}
            <TabView
              useNativeDriver
              navigationState={this.state}
              renderTabBar={this.renderTabBar}
              renderScene={this.renderScene}
              onIndexChange={index => this.setState({ index })}
              initialLayout={{ width: Dimensions.get('window').width }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    avatarContent: {
      resizeMode:"cover",
      height: 48,
      width: 48,
      borderRadius: 24,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
});
