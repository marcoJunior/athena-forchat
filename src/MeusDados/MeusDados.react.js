// Creditos do logo "Created by Freepik."
import React, { Component } from 'react';
import firebase  from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import { FloatingAction } from 'react-native-floating-action';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'

import {
    StyleSheet,
    Animated,
    Image,
    Linking,
    Text,
    View,
    ScrollView,
    Dimensions,
    Platform,
    Easing,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';

import { 
    Provider as PaperProvider,
    Appbar,
    Searchbar,
    Button,
    TextInput
} from 'react-native-paper';

import { COLOR, Avatar, Card, Icon, ListItem } from 'react-native-material-ui';
import { uiTheme } from '../App.react';

import logo from './../Imagens/logo-athena-qdr.png';
// import baner from './../Imagens/logo-athena-brnaco-fundo.png';
// import usuario from './../Imagens/user.png';

const { width, height } = Dimensions.get('window');
const UP = 1;
const DOWN = -1;

type Props = {
    drawer: any,
};

class MeusDados extends Component<Props> {
    constructor(props) {
        super(props);

        this.actions = [{
            text: 'Notificações',
            icon: <Icon name="notifications-active" color="white" size={24} />,
            name: 'notifications-active',
            position: 1
          }];

        this.state = {
            user: firebase.auth().currentUser,
            userdb: {},
            usuario: {},
            fadeAnim: new Animated.Value(1),
        };
        
        this.refUsuarios = firebase.firestore().collection('usuarios');
        this.storage = firebase.storage();
    }

    componentDidMount = async() => {
        await this.getInfoUsuario();
    }

    getInfoUsuario = async () => {
        const userdb = await this.refUsuarios.where('token','==', this.state.user.uid).get();
        const usuario = userdb.docs[0].data();
        this.setState({ usuario, userdb: userdb.docs[0] });
    } 

    selecionarFotoDeCapa = () => {
        const options = {
          title: 'Foto de Capa',
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
    
        ImagePicker.showImagePicker(options, async (response) => {
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            const { uri, path } = response;
            await ImageResizer.createResizedImage(uri, 750, 750, 'JPEG', 90, 0)
            .then(async imageCompress => {
                
                const file = await this.storage.ref(`usuarios/${this.state.user.uid}/photoCapaURL`)
                    .putFile(imageCompress.uri);

                this.refUsuarios.doc(this.state.userdb.id).update({
                    photoCapaURL: file.downloadURL,
                });

                await this.getInfoUsuario();
            })
            .catch(e => {
                console.warn('kkk', e);
            });

          }
        });
    }

    selecionarFotoDePerfil = () => {
        const options = {
          title: 'Foto de perfil',
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
    
        ImagePicker.showImagePicker(options, async (response) => {
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            const { uri, path } = response;
            await ImageResizer.createResizedImage(uri, 750, 750, 'JPEG', 90, 0)
            .then(async imageCompress => {
                
                const file = await this.storage.ref(`usuarios/${this.state.user.uid}/photoURL`)
                    .putFile(imageCompress.uri);

                this.refUsuarios.doc(this.state.userdb.id).update({
                    photoURL: file.downloadURL,
                });

                this.state.user.updateProfile({
                    photoURL: file.downloadURL || '',
                });
              
                await this.getInfoUsuario();
            })
            .catch(e => {
                console.warn('kkk',e.message);
            });
          }
        });
    }

    renderUserLogado = (usuario) => (
        <View style={[styles.containerImagemUser, { flex: 1, height: (height / 3) + 50 }]}>
            <TouchableOpacity
                style={{ width, height: (height / 3) }}
                onPress={this.selecionarFotoDeCapa}
            >
                <Image
                    flex={1}
                    resizeMode="cover" 
                    source={{ uri: usuario.photoCapaURL || 'https://firebasestorage.googleapis.com/v0/b/athena-forchat.appspot.com/o/logo-athena-qdr-br-1024x500.png?alt=media&token=1dc7c335-18d8-4b0e-bad2-909c1861df19' }}
                />
            </TouchableOpacity>
            <TouchableOpacity
                onPress={this.selecionarFotoDePerfil}
                style={{ top: -40 }}
            >
                <Avatar
                    xlarge
                    rounded
                    size={90}
                    iconSize={35}
                    icon={usuario.photoURL ? null : "camera-alt"}
                    image={
                        <Image
                            resizeMode="cover"
                            style={styles.imagemUser}
                            source={{ uri: usuario.photoURL ? usuario.photoURL : '' }}
                        />
                    }
                    // text={usuario.photoURL ? null : usuario.nome ? usuario.nome.substring(0,1) : null}
                    style={{
                        container: { backgroundColor: uiTheme.palette.secondaryColor },
                        content: {
                            fontWeight: 'bold',
                            fontSize: 24,
                        },
                    }}
                />
            </TouchableOpacity>           
        </View>
    )

    render = () => (
        <View testID="" flex={1} backgroundColor="#FFFFFF">
            <Appbar.Header>
                <Appbar.Action icon="menu" onPress={() => Actions.drawerOpen()} />
                <Appbar.Content
                    title="Meus dados"
                    subtitle={null}
                />
            </Appbar.Header>
                <ScrollView flex={1}>
                    <KeyboardAvoidingView
                        style={styles.container}
                        behavior="position"
                        keyboardVerticalOffset={Platform.OS === 'ios' ? 128 : 0}
                    >
                        
                        {this.renderUserLogado(this.state.usuario)}
                        <TextInput
                            label='Nome'
                            style={styles.input}
                            value={this.state.usuario.nome}
                            onChangeText={nome => {
                                this.state.usuario.nome = nome;
                                this.setState({ usuario: this.state.usuario });            
                            }}
                        />
                        <TextInput
                            label='Descrição'
                            style={styles.input}
                            value={this.state.usuario.descricao}
                            onChangeText={descricao => {
                                this.state.usuario.descricao = descricao;
                                this.setState({ usuario: this.state.usuario });
                            }}
                        />
                        <TextInput
                            label='Complementos'
                            style={styles.input}
                            value={this.state.usuario.complementos}
                            onChangeText={complementos => {
                                this.state.usuario.complementos = complementos;
                                this.setState({ usuario: this.state.usuario });
                            }}
                        />

                    </KeyboardAvoidingView>
                
                </ScrollView>

                <View style={{ padding: 5 }}>
                    <Button
                        primary
                        mode="contained"
                        styel={styles.btn}
                        onPress={() => {
                            this.state.user.updateProfile({
                                displayName: this.state.usuario.nome,
                            });
                            
                            this.refUsuarios.doc(this.state.userdb.id)
                                .update(this.state.usuario);

                            Actions.pop();
                            Actions.refresh();
                        }}
                    >
                        <Text style={{ fontSize: 18 }}>Editar</Text>
                    </Button>

                    {/* <FloatingAction
                        ref={(ref) => { this.floatingAction = ref; }}
                        actions={this.actions}
                        onPressItem={
                            (name) => {
                                console.log(`selected button: ${name}`);
                            }
                        }
                    /> */}
                </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    containerImagemUser: {
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    input: {
        marginHorizontal: 10,
        marginTop: 10,
        width: width - 30,
    },
    btn: {
        margin: 10,
    },
    imagemUser: {
        height: 90,
        width: 90,
        borderRadius: 45,
    },
    imagemItemActivit: {
        height: 60,
        width: 60,
        borderRadius: 30,
    },
});

export default MeusDados;
