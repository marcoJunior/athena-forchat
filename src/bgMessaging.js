// @flow
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
// Optional flow type
import type { RemoteMessage } from 'react-native-firebase';

export default async (message: RemoteMessage) => {
    // handle your message
    Actions.reset('subGrupos');
    return Promise.resolve();
}