import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import Stepper from 'react-native-js-stepper';
import { Actions } from 'react-native-router-flux';
import { StyleSheet, Image, Linking, Text, View, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Button,
    Toolbar, ToolbarBackAction, ToolbarContent, ToolbarAction
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import FormGrupo from './FormGrupo.react';
import ListaContatos from './ListaContatos.react';

const { width, height } = Dimensions.get('window');

type Props = {
    titulo: string,
    subgrupo: boolean,
    idGrupo: string,
};

export default class AddGrupos extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            loadAdd: false,
            showBottomStepper: true
        };

        this.tabela = this.props.subgrupo ? 'sub-grupos' : 'grupos';
        this.refGrupos = firebase.firestore().collection(this.tabela);
        this.storage = firebase.storage();
    }

    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
            <Toolbar>
                <ToolbarBackAction
                    onPress={() => Actions.pop()}
                />

                <ToolbarContent
                    title={this.props.titulo}
                    subtitle={null}
                />
            </Toolbar>

            <View flex={1}>

                <Stepper
                    ref={(ref: any) => {
                        this.stepper = ref
                    }}
                    initialPage={0}
                    validation={false}
                    activeDotStyle={styles.activeDot}
                    inactiveDotStyle={styles.inactiveDot}
                    showTopStepper={true}
                    showBottomStepper={this.state.showBottomStepper}
                    steps={['Descrição', 'Participantes']}
                    backButtonTitle="BACK"
                    nextButtonTitle="NEXT"
                    activeStepStyle={styles.activeStep}
                    inactiveStepStyle={styles.inactiveStep}
                    activeStepTitleStyle={styles.activeStepTitle}
                    inactiveStepTitleStyle={styles.inactiveStepTitle}
                    activeStepNumberStyle={styles.activeStepNumber}
                    inactiveStepNumberStyle={styles.inactiveStepNumber}
                >
                    <FormGrupo ref={(ref: any) => { this.formGrupo = ref }} subgrupo={this.props.subgrupo} />

                    <View flex={1}>
                        <ListaContatos ref={(ref: any) => { this.listaContatos = ref }} />
                        <Button
                            primary
                            raised
                            loading={this.state.loadAdd}
                            disabled={this.state.loadAdd}
                            onPress={async () => {
                                this.setState({ loadAdd: true });
                                const detalhes = this.formGrupo.state;
                                const listaParticipantes = this.listaContatos.state;
                                let obj = {
                                    nome: detalhes.nome,
                                    descricao: detalhes.descricao,
                                    photoURL: '',
                                    atividades: 0,
                                    usuarios: listaParticipantes.usuarios
                                };

                                let adicao = null;
                                if(detalhes.nome && detalhes.descricao && !this.props.subgrupo) {
                                    adicao = await this.refGrupos.add(obj);
                                } else if(detalhes.nome && detalhes.descricao && this.props.subgrupo) {
                                    obj['idGrupo'] = this.props.idGrupo;
                                    adicao = await this.refGrupos.add(obj);
                                }

                                if(adicao && detalhes.avatarSource) {
                                    let file = '';
                                    file = await this.storage.ref(`${this.tabela}/${adicao.id}`)
                                        .putFile(detalhes.avatarSource.uri);
                                        obj.photoURL = file.downloadURL;
                                    await this.refGrupos.doc(adicao.id).set(obj);
                                }

                                this.setState({ loadAdd: false });
                                Actions.pop();
                                Actions.refresh({ id: adicao.id });
                                return;
                            }}
                        >
                            <Text style={{ fontSize: 16 }}>CRIAR GRUPO</Text>
                        </Button>
                    </View>
                </Stepper>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
    activeDot: {
        backgroundColor: 'grey'
    },
    inactiveDot: {
        backgroundColor: '#ededed'
    },
    activeStep: {
        backgroundColor: 'grey'
    },
    inactiveStep: {
        backgroundColor: '#ededed'
    },
    activeStepTitle: {
        fontWeight: 'bold'
    },
    inactiveStepTitle: {
        fontWeight: 'normal'
    },
    activeStepNumber: {
        color: 'white'
    },
    inactiveStepNumber: {
        color: 'black'
    },
});
