import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import Stepper from 'react-native-js-stepper';
import { Actions } from 'react-native-router-flux';
import { StyleSheet, Image, Linking, Text, View, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Appbar,
    Searchbar,
    Button,
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';
import FormGrupo from './FormGrupo.react';
import ListaSelecao from './ListaSelecao.react';

const { width, height } = Dimensions.get('window');

type Props = {
    titulo: string,
    subgrupo: boolean,
    idGrupo: string,
};

export default class SelecionaParticipantes extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            isPesquisa: false,
            loadAdd: false,
            showBottomStepper: true
        };

        this.tabela = this.props.subgrupo ? 'sub-grupos' : 'grupos';
        this.refGrupos = firebase.firestore().collection(this.tabela);
        this.storage = firebase.storage();
    }

    renderAppbar = () => (
        <Appbar.Header>
            <Appbar.BackAction
                onPress={() => Actions.pop()}
            />

            <Appbar.Content
                title={this.props.titulo}
                subtitle={null}
            />
            <Appbar.Action icon="search" onPress={() => this.setState({ isPesquisa: true })} />
        </Appbar.Header>
    ) 

    renderBusca = () => (
        <View style={{ padding: 5, backgroundColor: theme.colors.primary }} >
            <Searchbar
                icon="arrow-back"
                placeholder="Pesquisa"
                onChangeText={pesquisa => { this.setState({ pesquisa }); }}
                value={this.state.pesquisa}
                onIconPress={() => this.setState({ isPesquisa: false })}
            />
        </View>
    )

    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
            {this.state.isPesquisa ? this.renderBusca() : this.renderAppbar()}

            <ListaSelecao
                flex={1}
                ref={(ref: any) => { this.listaContatos = ref }}
                pesquisa={this.state.pesquisa}
                subgrupo={this.props.subgrupo}
                idGrupo={this.props.idGrupo}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
    activeDot: {
        backgroundColor: 'grey'
    },
    inactiveDot: {
        backgroundColor: '#ededed'
    },
    activeStep: {
        backgroundColor: 'grey'
    },
    inactiveStep: {
        backgroundColor: '#ededed'
    },
    activeStepTitle: {
        fontWeight: 'bold'
    },
    inactiveStepTitle: {
        fontWeight: 'normal'
    },
    activeStepNumber: {
        color: 'white'
    },
    inactiveStepNumber: {
        color: 'black'
    },
});
