import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, Image, Linking, Text, View, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Searchbar,
    Button,
    Checkbox,
    Toolbar, ToolbarBackAction, ToolbarContent, ToolbarAction
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';
const { width, height } = Dimensions.get('window');

type Props = {};

export default class ListaContatos extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            usuarios: [
              firebase.auth().currentUser.uid,
            ],
            checked: {},
        };

        this.refUsuarios = firebase.firestore().collection('usuarios');
    }

    sleep = time => new Promise(resolve => setTimeout(() => resolve(), time))

    onFetch = async (page = 1, startFetch, abortFetch) => {
        try {
            let pageLimit = 24
            const skip = (page - 1) * pageLimit
    
            const usuarios = await this.refUsuarios.get();
            const data = usuarios.docs;

            // .filter((grupo) => grupo.data().usuarios.find(user => user == this.state.user.uid));
            // await this.sleep(2000)

            startFetch(data, pageLimit)
        } catch (err) {
            abortFetch() // manually stop the refresh or pagination if it encounters network error
            console.log(err)
        }
      }

    renderItem = (usuario, item2) => {
      return (
        <ListItem
            key={`grupo-${usuario.id}`}
            centerElement={{
                primaryText: usuario.data().nome,
                secondaryText: 'Informacoes complementares',
                tertiaryText: null
            }}
            leftElement={
              <Avatar 
                xlarge
                rounded
                size={45}
                text={usuario.data().photoURL ? null : 'G'}
                image={
                  usuario.data().photoURL ?
                  <Image
                      style={styles.avatarContent}
                      source={{ uri: usuario.data().photoURL }}
                  />
                  : null 
                }
                style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 20 } }}
              />
            }
            rightElement={
              <Checkbox
                checked={this.state.checked[usuario.id]}
                onPress={() => {
                  this.state.checked[usuario.id] = !this.state.checked[usuario.id];
                  this.setState({ checked: this.state.checked });

                  if(this.state.checked[usuario.id]){
                    this.state.usuarios.push(usuario.id);
                  } else {
                    const index = this.state.usuarios.indexOf(usuario.id);
                    this.state.usuarios.splice(index, 1);
                  }    
                }}
              />
            }
            onPress={() => {
              this.state.checked[usuario.id] = !this.state.checked[usuario.id];
              this.setState({ checked: this.state.checked });
              
              if(this.state.checked[usuario.id]){
                this.state.usuarios.push(usuario.id);
              } else {
                const index = this.state.usuarios.indexOf(usuario.id);
                this.state.usuarios.splice(index, 1);
              }
        }}
        />
      )
    }

    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
            <View flex={1}>
                <Searchbar
                    placeholder="Pesquisa"
                    onChangeText={pesquisa => { 
                      this.setState({ pesquisa });
                      this.listView.refresh();
                    }}
                    value={this.state.pesquisa}
                />

                <UltimateListView
                    ref={ref => this.listView = ref}
                    key={'ultimatelistacontato'}
                    onFetch={this.onFetch}
                    keyExtractor={(item) => item} 
                    refreshableMode="basic" // basic or advanced
                    item={this.renderItem} // this takes three params (item, index, separator)       
                    displayDate
                    allLoadedText="Acabou 😉"
                    arrowImageStyle={{ width: 20, height: 20, resizeMode: 'contain' }}
                />
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    avatarContent: {
      resizeMode:"cover",
      height: 48,
      width: 48,
      borderRadius: 24,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
});
