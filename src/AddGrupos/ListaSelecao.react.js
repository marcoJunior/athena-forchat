import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, Image, Alert, Linking, ActivityIndicator, Text, View, ScrollView, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Searchbar,
    Button,
    Checkbox,
    Chip,
    Appbar
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';
const { width, height } = Dimensions.get('window');

type Props = {
  subgrupo: boolean,
  idGrupo: number,
};

export default class ListaContatos extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            usuarios: [],
            checked: {},
            chips: [],
        };

        this.refUsuarios = firebase.firestore().collection('usuarios');
    }

    componentDidMount = async() => {
      const usuario = await this.refUsuarios.where('token','==', this.state.user.uid).get();
      this.state.usuarios.push(usuario.docs[0].id);
      this.setState({ usuarios: this.state.usuarios });
    }

    componentWillReceiveProps = (nprops) => {
      const { pesquisa } = nprops;
      if(pesquisa !== null) {
          this.setState({ pesquisa });
          this.listView.refresh();
      }
    } 

    sleep = time => new Promise(resolve => setTimeout(() => resolve(), time))

    onFetch = async (page = 1, startFetch, abortFetch) => {
        try {
          let pageLimit = 24
          const skip = (page - 1) * pageLimit;

          const usuario = await this.refUsuarios.where('token','==', this.state.user.uid).get();
          const findMeusAmigos = usuario.docs[0].data().amigos || [];

          const usuarios = await this.refUsuarios.get();
  
          const meusAmigos = usuarios.docs
              .filter(
                (user) => user.data().nome.indexOf(this.state.pesquisa) > -1
              )
              .filter(item => {
                  const { token } = item.data();
                  return findMeusAmigos.find(usr => usr === token );
              });
          
          startFetch(meusAmigos, pageLimit);
        } catch (err) {
            abortFetch();
            console.log(err)
        }
      }

    onPressUsuarios = (usuario) => {
      this.state.checked[usuario.id] = !this.state.checked[usuario.id];
      this.setState({ checked: this.state.checked });
      
      if(this.state.checked[usuario.id]){
        this.state.usuarios.push(usuario.id);
        this.state.chips.push(usuario);
      } else {
        const index = this.state.usuarios.indexOf(usuario.id);
        this.state.usuarios.splice(index, 1);

        const indexChip = this.state.chips.indexOf(usuario);
        this.state.chips.splice(indexChip, 1);
      }
    }

    renderItem = (usuario, item2) => {
      return (
        <ListItem
            key={`grupo-${usuario.id}`}
            centerElement={{
                primaryText: usuario.data().nome,
                secondaryText: 'Informacoes complementares',
                tertiaryText: null
            }}
            leftElement={
              <Avatar 
                xlarge
                rounded
                size={45}
                text={usuario.data().photoURL ? null : 'G'}
                image={
                  usuario.data().photoURL ?
                  <Image
                      style={styles.avatarContent}
                      source={{ uri: usuario.data().photoURL }}
                  />
                  : null 
                }
                style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 20 } }}
              />
            }
            rightElement={
              <Checkbox
                status={this.state.checked[usuario.id] ? 'checked' : 'unchecked'}
                onPress={() => this.onPressUsuarios(usuario)}
                // onPress={() => {
                //   this.state.checked[usuario.id] = !this.state.checked[usuario.id];
                //   this.setState({ checked: this.state.checked });

                //   if(this.state.checked[usuario.id]){
                //     this.state.usuarios.push(usuario.id);
                //     this.state.chips.push(usuario.data());
                //   } else {
                //     const index = this.state.usuarios.indexOf(usuario.id);
                //     this.state.usuarios.splice(index, 1);
                //     this.state.chips.pop();
                //   }
                // }}
              />
            }
            onPress={() => this.onPressUsuarios(usuario)}
        />
      )
    }

    renderEmptyView = () => (
      <View style={styles.container}>
          <Icon name={'search'} color={theme.colors.primary} size={65} />
          <Text>Nada encontrado!</Text>
      </View>
  )

  loadOpen = () => (
      <View style={styles.container}>
          <ActivityIndicator size="large" color={theme.colors.primary} />
          <Text>Aguarde...</Text>
      </View>
  )

  render = () => (
    <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
      <View style={{ flex: 1, backgroundColor: 'white', padding: 5 }}>
        <View flex={1}>
          <UltimateListView
              ref={ref => this.listView = ref}
              key={'ultimatelistacontato'}
              onFetch={this.onFetch}
              keyExtractor={(item) => item.id} 
              refreshableMode="basic" // basic or advanced
              item={this.renderItem} // this takes three params (item, index, separator)       
              displayDate
              allLoadedText="Acabou 😉"
              arrowImageStyle={{ width: 20, height: 20, resizeMode: 'contain' }}
          />
        </View>

        <Button
            primary
            mode="contained"
            styel={styles.btn}
            onPress={() => {

              if(this.state.usuarios.length > 1){
                Actions.addGruposForm({
                  idGrupo: this.props.idGrupo,
                  subgrupo: this.props.subgrupo,
                  titulo: 'Detalhes do grupo',
                  usuarios: this.state.usuarios,
                  chips: this.state.chips
                });
                return;
              }

              Alert.alert(
                'Ops!',
                'Selecione ao menos um amigo para criar um novo grupo!',
                [
                  {text: 'OK', onPress: async () => {}},
                ],
                { cancelable: false }
              );


            }}
        >
            <Text style={{ fontSize: 18 }}>SELECIONAR</Text>
        </Button>

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    avatarContent: {
      resizeMode:"cover",
      height: 48,
      width: 48,
      borderRadius: 24,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
});
