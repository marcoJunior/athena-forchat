import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import Stepper from 'react-native-js-stepper';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';
import { StyleSheet, Image, Linking, Text, View, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Button,
    Searchbar,
    Appbar
   } from 'react-native-paper';
import { theme } from '../App.react';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import ListaContatos from './ListaContatos.react';

const { width, height } = Dimensions.get('window');

type Props = {
    titulo: string
};

export default class Contatos extends Component<Props> {
    constructor(props) {
        super(props);

        this.actions = [{
            text: 'Add contato',
            icon: <Icon name="person-add" color="white" size={24} />,
            name: 'user',
            position: 1
          },
        ];

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            showBottomStepper: true
        };

        this.refGrupos = firebase.firestore().collection('grupos');
    }

    pressItem = (name) => {
        switch (name) {
            case 'user':
                Actions.addAmigos({ titulo: 'Add Amigos' });
                break;
            default:
                console.log(`selected button: ${name}`);
                break;
        }
    }

    renderAppbar = () => (
        <Appbar.Header>
            <Appbar.Action icon="menu" onPress={() => Actions.drawerOpen()} />

            <Appbar.Content
                title="Contatos"
                subtitle={null}
            />
            <Appbar.Action icon="search" onPress={() => this.setState({ isPesquisa: true })} />
        </Appbar.Header>
    ) 

    renderBusca = () => (
        <View style={{ padding: 5, backgroundColor: theme.colors.primary }} >
            <Searchbar
                icon="arrow-back"
                placeholder="Pesquisa"
                onChangeText={pesquisa => { this.setState({ pesquisa }); }}
                value={this.state.pesquisa}
                onIconPress={() => this.setState({ isPesquisa: false })}
            />
        </View>
    )

    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
            {this.state.isPesquisa ? this.renderBusca() : this.renderAppbar()}

            <View flex={1}>
                <ListaContatos
                    ref={(ref: any) => { this.listaContatos = ref }}
                    pesquisa={this.state.pesquisa}
                />
            </View>

            <FloatingAction
                ref={(ref) => { this.floatingAction = ref; }}
                actions={this.actions}
                onPressItem={this.pressItem}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
    activeDot: {
        backgroundColor: 'grey'
    },
    inactiveDot: {
        backgroundColor: '#ededed'
    },
    activeStep: {
        backgroundColor: 'grey'
    },
    inactiveStep: {
        backgroundColor: '#ededed'
    },
    activeStepTitle: {
        fontWeight: 'bold'
    },
    inactiveStepTitle: {
        fontWeight: 'normal'
    },
    activeStepNumber: {
        color: 'white'
    },
    inactiveStepNumber: {
        color: 'black'
    },
});
