import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import pull from 'lodash/pull';
import { Actions } from 'react-native-router-flux';
import { FloatingAction } from 'react-native-floating-action';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview'
import { StyleSheet, Image, Linking, Text, View, ActivityIndicator, Dimensions } from 'react-native';

import { 
    Provider as PaperProvider,
    Searchbar,
    Button,
    Checkbox,
    Appbar
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';
const { width, height } = Dimensions.get('window');

type Props = {
    pesquisa: string,
};

export default class ListaContatos extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            user: firebase.auth().currentUser,
            pesquisa: '',
            usuarios: [],
            checked: {},
        };

        this.refUsuarios = firebase.firestore().collection('usuarios');
    }

    componentWillReceiveProps = (nprops) => {
        const { pesquisa } = nprops;
        if(pesquisa !== null) {
            this.setState({ pesquisa });
            this.listView.refresh();
        }
    } 

    sleep = time => new Promise(resolve => setTimeout(() => resolve(), time))

    onFetch = async (page = 1, startFetch, abortFetch) => {
        try {
            let pageLimit = 24
            const skip = (page - 1) * pageLimit;

            const usuario = await this.refUsuarios.where('token','==', this.state.user.uid).get();
            const findMeusAmigos = usuario.docs[0].data().amigos || [];

            const usuarios = await this.refUsuarios.get();
    
            const meusAmigos = usuarios.docs
                .filter(
                  (user) => user.data().nome.indexOf(this.state.pesquisa) > -1
                )
                .filter(item => {
                    const { token } = item.data();
                    return findMeusAmigos.find(usr => usr === token );
                });
            
            startFetch(meusAmigos, pageLimit);
        } catch (err) {
            abortFetch();
            console.log('deu ruim', err)
        }
    }

    renderItem = (usuario, item2) => {
      return (
        <ListItem
            key={`grupo-${usuario.id}`}
            centerElement={{
                primaryText: usuario.data().nome,
                secondaryText: usuario.data().descricao || 'Informacoes complementares',
                tertiaryText: null
            }}
            leftElement={
              <Avatar 
                xlarge
                rounded
                size={45}
                text={usuario.data().photoURL ? null : usuario.data().nome.substring(0,1)}
                image={
                  usuario.data().photoURL ?
                  <Image
                      style={styles.avatarContent}
                      source={{ uri: usuario.data().photoURL }}
                  />
                  : null 
                }
                style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 20 } }}
              />
            }
            onPress={() => {
              console.warn('oie');  
            }}
        />
      )
    }

    renderEmptyView = () => (
        <View style={[styles.container, { height: height / 1.3 }]}>
            <Icon name={'search'} color={theme.colors.primary} size={65} />
            <Text>Nada encontrado!</Text>
        </View>
    )

    loadOpen = () => (
        <View style={[styles.container, { height: height / 1.3 }]}>
            <ActivityIndicator size="large" color={theme.colors.primary} />
            <Text>Aguarde...</Text>
        </View>
    )

    render = () => (
        <View key={'chat-list'} testID="" flex={1} backgroundColor="#FFFFFF">
            <View flex={1}>
                <UltimateListView
                    pagination={false}
                    autoPagination={false}
                    ref={ref => this.listView = ref}
                    key={'ultimatelistacontato'}
                    onFetch={this.onFetch}
                    keyExtractor={(item) => item.id} 
                    refreshableMode="basic"
                    item={this.renderItem}
                    displayDate
                    allLoadedText="Acabou 😉"
                    arrowImageStyle={{ width: 20, height: 20, resizeMode: 'contain' }}

                    paginationFetchingView={this.loadOpen}
                    emptyView={this.renderEmptyView}
                />
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
    },
    avatarContent: {
      resizeMode:"cover",
      height: 48,
      width: 48,
      borderRadius: 24,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.7,
        backgroundColor: 'white',
        width,
        height,
    },
});
