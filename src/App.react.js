import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import { GoogleSignin } from 'react-native-google-signin';
  
import Snackbar from 'react-native-snackbar';

import { Overlay, Router, Stack, Drawer, Tabs, Scene, Actions, ActionConst, Reducer } from 'react-native-router-flux';
import { 
  DefaultTheme, DarkTheme, Provider as PaperProvider,
  Button,
  Drawer as DrawerPaper,
} from 'react-native-paper';
import { COLOR, Drawer as DrawerMaterial, Avatar, Icon, ThemeContext, getTheme } from 'react-native-material-ui';
import { StyleSheet, Text, View, ScrollView, Image, StatusBar, Linking, Platform, BackHandler } from 'react-native';

import DeviceInfo from 'react-native-device-info';

// import baner from './Imagens/baner.png';
// import usuario from './Imagens/user.png';
import baner from './Imagens/logo-athena-qdr-br-1024x500.png';

import Introducao from './Iniciar/Introducao.react';
import Iniciar from './Iniciar';
import Login from './Login';
import Web from './Web';
import Cadastro from './Cadastro';
import Grupos from './Grupos';
import Contatos from './Contatos';
import AddAmigos from './AddAmigos';
import AddGrupos from './AddGrupos';
import EditGrupos from './EditGrupos';
import AddGruposForm from './AddGrupos/FormGrupo.react';
import SubGrupos from './SubGrupos';
import Pergunta from './Pergunta';
import MeusDados from './MeusDados';
import { primaria, acentuada, adMobi } from './constantes';

global.Authorization = '';

export const uiTheme = {
  drawerHeaderListItem: {
    primaryText: { color: 'white' },
    secondaryText: { color: 'white' },
  },
  palette: {
      primaryColor: primaria,
      secondaryColor: acentuada,
  },
  toolbar: {
      container: {
          height: 50,
      },
  },
};

export const theme = {
  ...DefaultTheme,
  roundness: 4,
  colors: {
    ...DefaultTheme.colors,
    primary: primaria,
    accent: acentuada,
  },
};

export const darkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    primary: primaria,
    accent: acentuada,
  },
};

global.drawer = false;

console.disableYellowBox = true;

export const criarPropaganda = (actions = () => {}) => {
    const Banner = firebase.admob.Banner;
    const AdRequest = firebase.admob.AdRequest;

    const advertLocal = firebase.admob().rewarded('ca-app-pub-5152531393270719/8758938338');
    advertLocal.loadAd(new AdRequest().addTestDevice().build());

    advertLocal.on('onAdFailedToLoad', (e) => {
        actions();
    });

    advertLocal.on('onAdLoaded', () => {
        if(advertLocal.isLoaded()){
            advertLocal.show();
            return;
        }
        actions();
    });

    advertLocal.on('onRewarded', (event) => {
        actions();
    });
}

export const sleep = time => new Promise(resolve => setTimeout(() => resolve(), time))

type Props = {};
export default class App extends Component<Props> {
    constructor() {
        super();

        this.unsubscriber = null;
        this.state = {
            visible: false
        };

        Actions.active = 'chats';
        Actions.backButtonPressedOnceToExit = false;

        this.refMensagens = firebase.firestore().collection('mensagens');
    }

    componentDidMount = async() => {
        await GoogleSignin.configure({
            iosClientId: '571777234453-t4ojk5tud1gtd3geeis44upeg125dg9f.apps.googleusercontent.com'
        })

        BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid.bind(this));
        this.unsubscriber = firebase.auth().onAuthStateChanged(user => Actions.user = user);
        await sleep(100);
        Actions.user = firebase.auth().currentUser;
        await this.setServicosFcm();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid.bind(this));
        if (this.unsubscriber) {
          this.unsubscriber();
        }
    }

    onBackAndroid() {
        const currentSceneName = Actions.currentScene;
        
        if (Actions.backButtonPressedOnceToExit) {
            BackHandler.exitApp();
        } else {
            if ([
              'login',
              '_grupos',
              '_meusDados',
              '_contatos',
              '_web',
            ].indexOf(currentSceneName) === -1) {
                Actions.pop();
                return true;
            } else {
                Actions.backButtonPressedOnceToExit = true;
                // Pressione novamente para sair do App!

                Snackbar.show({
                    title: 'Pressione novamente para sair do App!',
                    duration: Snackbar.LENGTH_LONG,
                  });
                
                setTimeout(function() {
                    Actions.backButtonPressedOnceToExit = false;
                }, 2000);
                return true;
            }
        }

    }

    getSceneStyle = () => ({
        shadowOpacity: 0,
        shadowRadius: 3,
    });

    // firebase.messaging().subscribeToTopic('topico-de-teste');
    setServicosFcm = async () => {
        try {
            const enabled = await firebase.messaging().hasPermission();
            if (!enabled) {
                try {
                    await firebase.messaging().requestPermission();
                } catch (error) {
                    return false;
                }
            }

            this.notificationListener = firebase.notifications().onNotification(
                (notification) => {
                    console.warn(notification.data);
                    if (Platform.OS === 'android') {
                        const channel = new firebase.notifications.Android.Channel(
                            'geral',
                            'Notificação',
                            firebase.notifications.Android.Importance.Max
                        )
                            .setDescription('Novo canal de notificação app android!');

                        firebase.notifications().android.createChannel(channel);

                        notification.android.setChannelId('geral')
                            .android.setSmallIcon('ic_notificacao');
                        notification.color = theme.colors.primary;
                    }

                    firebase.notifications().displayNotification(notification);

                    if(notification.data.messageId) {
                        this.refMensagens.doc(notification.data.messageId)
                            .update({ received: true });
                    }

                    Actions.refresh({ refresh: Math.random(), notification });
                }
            );

            // this.messageListener = firebase.messaging().onMessage((message) => {
            //     console.warn(message);
            // });

        } catch (error) {
            console.warn(error);
        }
    }

    reducerCreate = (params) => {
        const defaultReducer = new Reducer(params);
        return (state, action) =>
            // console.log('ACTION:', state);
            defaultReducer(state, action);
    };

    renderAvatar = (tipo) => (
        tipo === 'texto' ?
        <Avatar 
            xlarge
            rounded
            text={Actions.user && Actions.user.displayName ? Actions.user.displayName.substring(0,1) : 'U'}
            style={{ content: { fontSize: 20 } }}
        />
        :
        <Avatar
            xlarge
            rounded
            image={
                <Image
                    resizeMode="cover"
                    style={styles.avatarContent}
                    source={{ uri: Actions.user.photoURL ? Actions.user.photoURL : null }}
                />
            }
        />
    )

    renderTopSidebar = () => (
        <DrawerMaterial.Header
            // image={<Image source={} style={{ flex: 1, resizeMode: 'stretcoverch' }}/>}
            style={{ 
                container: { backgroundColor: 'rgba(64, 64, 64, 0.8)' }, 
                contentContainer: { backgroundColor: uiTheme.palette.primaryColor } 
            }}
        >
            <DrawerMaterial.Header.Account
                avatar={this.renderAvatar(Actions.user && Actions.user.photoURL ? 'imagem' : 'texto')}
                footer={{
                    dense: true,
                    centerElement: {
                        primaryText: `Olá ${Actions.user && Actions.user.displayName ? Actions.user.displayName : 'seja bem vindo!'}`,
                        secondaryText: Actions.user ? Actions.user.email : null,
                    }
                }}
            />
        </DrawerMaterial.Header>
    )

    renderSectionUsuario = (active) => (
        <DrawerPaper.Section title="Principal">
            <DrawerPaper.Item
                icon="message"
                label="Chats"
                active={active === 'chats'}
                onPress={() => { 
                    Actions.active = 'chats'; 
                    // Actions.drawerClose();
                    Actions.grupos({ });
                    Actions.refresh({});
                    Actions.drawerClose();
                }}
            />
            <DrawerPaper.Item
                icon="contacts"
                label="Contatos"
                active={active === 'contacts'}
                onPress={() => {
                    Actions.active = 'contacts';
                    // Actions.drawerClose();
                    Actions.refresh({});
                    Actions.contatos({ });
                    Actions.drawerClose();
                }}
            />
            <DrawerPaper.Item
                icon="person"
                label="Meus dados"
                active={active === 'person'}
                onPress={() => {
                    Actions.active = 'person';
                    // Actions.drawerClose();
                    Actions.refresh({});
                    Actions.meusDados();
                    Actions.drawerClose();
                }}
            />
        </DrawerPaper.Section>
    )

    renderDrawerItemWeb = (active, icon, label, chave) => (
        <DrawerPaper.Item
            icon={icon}
            label={label}
            active={active === chave}
            onPress={() => {
                Actions.active = chave;
                Actions.web({});
                setTimeout(() => {
                    Actions.refresh({
                        url: `https://athena-forchat.firebaseapp.com/${chave}`,
                        titulo: label,
                    });
                    Actions.drawerClose();
                }, 0);
            }}
        />
    )

    renderSectionGeral = (active) => (
        <DrawerPaper.Section title="Principal">
            <DrawerPaper.Item
                icon="event-available"
                label="Avaliar"
                active={active === 'event-available'}
                onPress={() => {
                    Actions.active = 'event-available';
                    Linking.openURL(
                        `https://play.google.com/store/apps/details?id=${DeviceInfo.getBundleId()}`
                    );
                }}
            />

            {this.renderDrawerItemWeb(active, 'gavel', 'Termos de uso', 'termos-de-uso')}
            {this.renderDrawerItemWeb(active, 'find-in-page', 'Política de privacidade', 'politica-de-privacidade')}
            {/* {this.renderDrawerItemWeb(active, 'work', 'Sobre o app', 'sobre-o-app')} */}
            {this.renderDrawerItemWeb(active, 'code', 'Por TPWS', 'por-tpws')}
            {this.renderDrawerItemWeb(active, 'new-releases', `Versão ${DeviceInfo.getVersion()}`, 'sobre-versoes')}

             <DrawerPaper.Item
                icon="exit-to-app"
                label="Sair"
                onPress={() => {
                    firebase.auth().signOut();
                    GoogleSignin.signOut()
                        .then(() => {
                            console.log('out');
                        })
                        .catch((err) => {
                            console.log(err);
                        });
        
                    Actions.login({});
                }}
            />

        </DrawerPaper.Section>
    )

   
    
    renderSideBar = (active) => (
        <View flex={1}>
            {this.renderTopSidebar()}
            <ScrollView>
                {this.renderSectionUsuario(active)}
                {this.renderSectionGeral(active)}
            </ScrollView>
        </View>
    )

    render() {
        return (
            <PaperProvider theme={theme}>
                <ThemeContext.Provider value={getTheme(uiTheme)}>
                    <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)" translucent barStyle="light-content" />
                    <View style={{ backgroundColor: theme.colors.primary,  height: 24 }} />
                    <Router
                        backAndroidHandler={global.backPress}
                        createReducer={this.reducerCreate}
                        getSceneStyle={this.getSceneStyle}
                    >
                        <Overlay key="overlay">
                            <Stack key="root" tintColor="#FFFFFF" navigationBarStyle={{ backgroundColor: uiTheme.palette.primaryColor }}>

                                <Scene
                                    key="iniciar" 
                                    component={Iniciar}
                                    title="Iniciar" 
                                    hideNavBar
                                    type={ActionConst.RESET}
                                />

                                <Scene
                                    key="introducao" 
                                    component={Introducao}
                                    title="Introducao" 
                                    hideNavBar
                                    type={ActionConst.RESET}
                                />

                                <Scene
                                    key="login" 
                                    component={Login}
                                    title="login" 
                                    hideNavBar
                                    type={ActionConst.RESET}
                                />

                                <Scene
                                    key="cadastro" 
                                    component={Cadastro}
                                    title="Cadastro" 
                                    hideNavBar
                                />

                                <Drawer
                                    key="rootDrawer"
                                    hideNavBar
                                    contentComponent={() => this.renderSideBar(Actions.active)}
                                    openDrawerOffset={0.2}
                                    tapToClose={true}
                                >

                                    <Scene
                                        key="grupos"
                                        component={Grupos}
                                        title="Grupos"
                                        hideNavBar
                                    />

                                    <Scene
                                        key="contatos"
                                        component={Contatos}
                                        title="Contatos"
                                        hideNavBar
                                    />

                                    <Scene
                                        key="meusDados"
                                        component={MeusDados} 
                                        title="Meus dados"
                                        hideNavBar
                                    />

                                    <Scene
                                        key="web" 
                                        component={Web}
                                        title="web" 
                                        hideNavBar
                                    />

                                </Drawer>

                                <Scene
                                    key="addAmigos"
                                    component={AddAmigos}
                                    title="AddAmigos"
                                    hideNavBar
                                />

                                <Scene
                                    key="addGrupos"
                                    component={AddGrupos}
                                    title="AddGrupos"
                                    hideNavBar
                                />

                                <Scene
                                    key="editGrupos"
                                    component={EditGrupos}
                                    title="EditGrupos"
                                    hideNavBar
                                /> 

                                <Scene
                                    key="subGrupos"
                                    component={SubGrupos}
                                    title="SubGrupos"
                                    hideNavBar
                                />

                                <Scene
                                    key="pergunta"
                                    component={Pergunta}
                                    title="Pergunta"
                                    hideNavBar
                                />

                                <Scene
                                    key="addGruposForm"
                                    component={AddGruposForm}
                                    title="AddGruposForm"
                                    hideNavBar
                                /> 

                            </Stack>
                        </Overlay>
                    </Router>

                </ThemeContext.Provider>
            </PaperProvider>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  containerDrawer: {
    flex: 1,
    backgroundColor: 'white',
  },
  avatarContent: {
    height: 55,
    width: 55,
    borderRadius: 27.5,
  },
  imagemUser: {
    height: 90,
    width: 90,
    borderRadius: 45,
},
});
