import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
import Stepper from 'react-native-js-stepper';
import { Actions } from 'react-native-router-flux';
import {
  StyleSheet,
  Alert,
  Image,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from 'react-native';

import { 
    Provider as PaperProvider,
    Appbar,
    Button,
    Card,
    Divider,
    TextInput,
    Subheading,
    Chip
   } from 'react-native-paper';
import { COLOR, Avatar, Badge, Icon, ListItem } from 'react-native-material-ui';
import { theme } from '../App.react';

const { width, height } = Dimensions.get('window');

type Props = {
    titulo: string,
    idGrupo: number,
    grupo: any,
    subgrupo: boolean,
    usuarios: array,
    chips: array,
};

export default class FormGrupo extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            loadAdd: false,
            user: firebase.auth().currentUser,
            chips: this.props.chips,
            nome: this.props.grupo.nome,
            descricao: this.props.grupo.descricao,
            avatarSource: { uri: this.props.grupo.photoURL },
        };

        this.telaTipo = this.props.subgrupo ? 'Subgrupo' : 'Grupo';

        this.tabela = this.props.subgrupo ? 'sub-grupos' : 'grupos';
        this.refGrupos = firebase.firestore().collection(this.tabela);
        this.storage = firebase.storage();
    }

    criarGrupo = async () => {
      const { nome, descricao, avatarSource  } = this.state;

      if(!nome && !descricao) {
        Alert.alert(
          'Ops!',
          'Preencha um nome e uma descrição !',
          [
            {text: 'OK', onPress: async () => {}},
          ],
          { cancelable: false }
        );

        return;
      }

      this.setState({ loadAdd: true });
    
      let obj = {
          nome,
          descricao,
          photoURL: '',
          atividades: 0,
          usuarios: this.props.usuarios,
          criador: this.state.user.uid,
      };

      let adicao = null;
      if(nome && descricao && this.props.subgrupo) {
        obj['idGrupo'] = this.props.idGrupo;
        adicao = await this.refGrupos.add(obj);
      } else if(nome && descricao && !this.props.subgrupo) {
        adicao = await this.refGrupos.add(obj);
      } 

      if(adicao && avatarSource) {
          let file = '';
          file = await this.storage.ref(`${this.tabela}/${adicao.id}`)
              .putFile(avatarSource.uri);
              obj.photoURL = file.downloadURL;
          await this.refGrupos.doc(adicao.id).set(obj);
      }

      Actions.pop();
      Actions.pop();
      if(nome && descricao && this.props.subgrupo) {
        Actions.pop();
      }
      setTimeout(() => {
        Actions.refresh({ id: adicao.id });        
      }, 100);
      this.setState({ loadAdd: false });

      return;
  }

    selecionarFotoDePerfil = () => {
        const options = {
          title: 'Select Avatar',
        //   customButtons: [
        //     {name: 'fb', title: 'Choose Photo from Facebook'},
        //   ],
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
        
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        
            this.setState({
              avatarSource: source
            });
          }
        });
    }

    renderAvatar = (source) => (
        <TouchableOpacity
          flex={1}
          onPress={this.selecionarFotoDePerfil}
          style={{ marginTop: 20 }}
        >
          <Avatar 
              xlarge
              rounded
              size={60}
              icon={source ? null : "camera-alt"}
              // text={source ? null : 'G'}
              image={
                source ?
                <Image
                    style={styles.avatarContent}
                    source={source}
                />
                : null 
              }
              style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 22 } }}
          />
        </TouchableOpacity>
    )

    renderAppbar = () => (
      <Appbar.Header>
          <Appbar.BackAction
              onPress={() => Actions.pop()}
          />

          <Appbar.Content
              title={this.props.titulo}
              subtitle={null}
          />

      </Appbar.Header>
    )

    renderUsuariosSelecionados = () => (
      <View style={{ flex: 1, paddingHorizontal: 15 }} >
        <Subheading>Participantes</Subheading>
        <ScrollView horizontal style={{ maxHeight: 45, flexDirection: 'row', padding: 5 }}>
          {
            this.state.chips.map(
              usuario => 
                <Chip
                  avatar={<Image source={{ uri: usuario.data().photoURL }} />}
                  onPress={() => console.log('Pressed')}
                  onClose={() => {
                    const indexChip = this.state.chips.indexOf(usuario);
                    this.state.chips.splice(indexChip, 1);
                    this.setState({ chips: this.state.chips });

                    if(this.state.chips.length == 0){
                      Actions.pop();
                      Actions.refresh({});
                    }
                  }}
                >
                  {usuario.data().nome}
                </Chip>
            )
          }
        </ScrollView>
      </View>
    )

    render = () => (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {this.renderAppbar()}
        
        <View style={{ flex: 1, backgroundColor: 'white', padding: 5 }}>
          <View style={styles.container}>
            <View style={{ flexDirection: 'row', margin: 10 }}>
                {this.props.subgrupo ? null : this.renderAvatar(this.state.avatarSource)}
                <KeyboardAvoidingView
                    style={styles.container}
                    behavior={Platform.OS === 'ios' ? 'padding' : null}
                    keyboardVerticalOffset={Platform.OS === 'ios' ? 128 : 0}
                >
                    <TextInput
                        label={`Nome do ${this.telaTipo}`}
                        style={styles.input}
                        value={this.state.nome}
                        onChangeText={nome => this.setState({ nome })}
                    />
                    <TextInput
                        label={`Descrição do ${this.telaTipo}`}
                        style={styles.input}
                        value={this.state.descricao}
                        onChangeText={descricao => this.setState({ descricao })}
                    />
                </KeyboardAvoidingView>
            </View>

            <Divider style={{ height: 3 }} />

            {/* {this.renderUsuariosSelecionados()} */}

          </View>

          <Button
            primary
            mode="contained"
            styel={styles.btn}
            loading={this.state.loadAdd}
            disabled={this.state.loadAdd}
            onPress={() => {}}
          >
              <Text style={{ fontSize: 18 }}>EDITAR GRUPO</Text>
          </Button>
        </View>

      </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
      },
      avatarContent: {
        resizeMode:"cover",
        height: 60,
        width: 60,
        borderRadius: 30,
      },
      logo: {
        flex: 1,
        resizeMode: 'contain',
      },
      input: {
        marginBottom: 10,
        marginHorizontal: 10,
        width: width - 100,
      },
      containerInputs: {
        marginVertical: 5,
        borderRadius: 5,
      },
      botaoLogar: {
        height: 42,
        width: width - 10,
      },
});
