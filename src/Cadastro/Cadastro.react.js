import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import { SocialIcon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import { Avatar } from 'react-native-material-ui';

import {
  DefaultTheme,
  Appbar,
  Card,
  TextInput,
  Button,
  Divider,
  IconButton,
 } from 'react-native-paper';

import {
  StyleSheet,
  Alert,
  Image,
  Text,
  View,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity
} from 'react-native';

import Login from '../Login';

import logo from '../Imagens/logo.png';
import { theme } from '../App.react';
// import { getTokenLogin } from '../fetchs/Autenticacao.react';
// import { getUsuarioLogado } from '../fetchs/Usuarios.react';

const window = Dimensions.get('window');
export const IMAGE_HEIGHT = window.width / 2;
export const IMAGE_HEIGHT_SMALL = window.width /7;

type Props = {};
export default class Cadastro extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      isLoad: false,
      nome: '',
      email: '',
      senha: '',
      avatarSource: null,
      secureTextEntry: true,
    };

    this.refUsuarios = firebase.firestore().collection('usuarios');
    this.storage = firebase.storage();
  }

  getMensagemErroCadastro = (code) => {
    switch (code) {
      case 'auth/invalid-email':
        return 'Email invalido!';
        break;
      case 'auth/email-already-in-use':
        return 'Já existe uma conta com este e-mail.';
        break;
      case 'auth/operation-not-allowed':
        return 'Usuario desativado.';
        break;
      case 'auth/weak-password':
        return 'Sua senha não é forte o suficiente!';
        break;
      
      default:
        return 'Não foi possivel identificar o que aconteceu!\nTente novamente em alguns instantes!';
        break;
    }
  }

  criarUsuario = async (usuario, senha) => {
    try {
      const cadastro = await firebase.auth().createUserWithEmailAndPassword(usuario, senha);
      cadastro.displayName = this.state.nome;

      let file = '';
      if(this.state.avatarSource){
        file = await this.storage.ref(`usuarios/${cadastro.uid}/perfil`).putFile(this.state.avatarSource.uri);
      }

      // cadastro.updateProfile({
      //   displayName: this.state.nome,
      //   photoURL: file.downloadURL || '',
      // });

      this.refUsuarios.add({
        photoURL: file.downloadURL || '',
        nome: this.state.nome,
        token: cadastro.uid,
        amigos: [],
      });

      cadastro.sendEmailVerification();
      
      Alert.alert(
        'Agora falta pouco!',
        'Você devera receber um e-mail de confimação!\nOlhe também na caixa de spans!',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      );
    } catch (error) {
      const msg = this.getMensagemErroCadastro(error.code);

      Alert.alert(
        'Ops!',
        msg,
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
      return false;
    }
  }

  selecionarFotoDePerfil = () => {
    const options = {
      title: 'Select Avatar',
      // customButtons: [
      //   {name: 'fb', title: 'Choose Photo from Facebook'},
      // ],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
  
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        this.setState({
          avatarSource: source
        });
      }
    });
  }

  renderAvatar = (source) => (
    <View style={{
      flex: 1,
      margin: 8,
    }}>
    <TouchableOpacity
      onPress={this.selecionarFotoDePerfil}
    >
      <Avatar 
          xlarge
          rounded
          size={80}
          iconSize={35}
          // text={source ? null : 'U'}
          icon={source ? null : "camera-alt"}
          image={
            source ?
            <Image
                style={styles.avatarContent}
                source={source}
            />
            : null 
          }
          style={{ container: { backgroundColor: theme.colors.accent }, content: { fontSize: 35 } }}
      />
    </TouchableOpacity>
    </View>
  )

  rendeFromulario = () => (
    <View style={{ flex: 1, marginHorizontal: 10 }}>      
      <Card>
        <Card.Content>
          {/* <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {this.renderAvatar(this.state.avatarSource)}
          </View> */}
          <KeyboardAvoidingView
            flex={1}
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 0}
          >
            <TextInput
              label='Nome completo'
              style={[styles.input, { marginTop: 10 }]}
              value={this.state.nome}
              onChangeText={nome => this.setState({ nome })}
            />
            <TextInput
              label='E-mail'
              style={styles.input}
              keyboardType="email-address"
              value={this.state.email}
              onChangeText={email => this.setState({ email })}
            />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <TextInput
                secureTextEntry={this.state.secureTextEntry}
                label='Senha'
                style={styles.input}
                value={this.state.senha}
                onChangeText={senha => this.setState({ senha })}
              />

              <IconButton
                icon={this.state.secureTextEntry ? 'visibility-off' : 'visibility'}
                color={this.state.secureTextEntry ? theme.colors.disabled : theme.colors.primary}
                size={28}
                onPress={() => this.setState({ secureTextEntry: !this.state.secureTextEntry })}
              />
            </View>
          </KeyboardAvoidingView>

          <Button
              primary
              mode="contained"
              loading={this.state.isLoad}
              disabled={this.state.isLoad}
              style={styles.botaoLogar}
              onPress={async () => {
                const { nome, email, senha } = this.state;
                if(!nome && !email && !senha){
                  Alert.alert(
                    'Ops!',
                    'Preencha todos os campos!',
                    [
                      {text: 'OK', onPress: async () => {}},
                    ],
                    { cancelable: false }
                  );
                  return;
                }

                this.setState({ isLoad: true });
                await this.criarUsuario(email, senha);
                this.setState({ isLoad: false });
              }}
            >
              <Text style={{ fontSize: 18 }}>ME CADASTRAR</Text>
            </Button>
        </Card.Content>
      </Card>
    </View>
  )

  render() {
    return (
      <View flex={1} backgroundColor="#F5F5F5">
        <Appbar.Header>
          <Appbar.BackAction
            onPress={() => Actions.pop()}
          />

          <Appbar.Content
              title="Cadastro "
              subtitle={null}
          />
        </Appbar.Header>

          <ScrollView marginTop={15}>
            {this.rendeFromulario()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarContent: {
    resizeMode:"cover",
    height: 80,
    width: 80,
    borderRadius: 40,
  },
  logo: {
    flex: 1,
    resizeMode: 'contain',
  },
  input: {
    flex: 1,
    marginBottom: 10,
  },
  containerInputs: {
    marginVertical: 5,
    borderRadius: 5,
  },
  botaoLogar: {
    flex: 1,
    height: 42,
  },
  buttonLogarComFacebook: {
    height: 52,
    borderRadius: 2,
    paddingHorizontal: 10,
  },
  buttonLogarComGoogle: {
      height: 52,
      borderRadius: 2,
      paddingHorizontal: 10,
  },
});
