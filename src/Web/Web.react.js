import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import {
    StyleSheet,
    ActivityIndicator,
    Image,
    Text,
    View,
    Dimensions,
    Platform,
    WebView,
} from 'react-native';

import { 
    Provider as PaperProvider,
    Appbar
} from 'react-native-paper';

import { uiTheme, theme } from '../App.react';

const { width, height } = Dimensions.get('window');

type Props = {
    url: string,
    titulo: string,
    subtitulo?: string,
};

export default class Web extends Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            titulo: this.props.titulo,
            subtitulo: this.props.subtitulo,
            url: this.props.url,
        };
    }
    
    componentWillReceiveProps = (nprops) => {
        const { titulo, subtitulo, url } = nprops;
        this.setState({ titulo, subtitulo, url });
    } 

    renderLoading = () => (
        <View style={styles.container}>
            <ActivityIndicator size="large" color={theme.colors.primary} />
            <Text>Aguarde...</Text>
        </View>
    )

    render = () => (
        <View flex={1} backgroundColor="#FFFFFF">
            <Appbar.Header>
                <Appbar.Action icon="menu" onPress={() => Actions.drawerOpen()} />
                <Appbar.Content
                    title={this.state.titulo}
                    subtitle={this.state.subtitulo || null}
                />
            </Appbar.Header>
            <View flex={1}>
                
                <WebView
                    source={{ uri: this.state.url }}
                    renderLoading={this.renderLoading} 
                    startInLoadingState={true}  
                />
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    containerImagemUser: {
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
});
