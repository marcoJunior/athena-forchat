import React, {Component} from 'react';

import firebase from 'react-native-firebase';

import { Actions } from 'react-native-router-flux';
import { 
  Provider as PaperProvider,
  Searchbar,
  Button,
  Appbar
 } from 'react-native-paper';
import {StyleSheet, Text, View} from 'react-native';


type Props = {
  drawerApp: any,
};

export default class Home extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      usuario: firebase.auth().currentUser,
      isSearch: false,
      firstQuery: '',
    };

  }

  clickPre = async () => {
    // console.warn(docs.query);

    // docs.forEach((snapshot: DocumentSnapshot) => {
    //   console.warn(snapshot);
    // });


    // this.ref.add({
    //   nome: 'Marco juniordsfmnbfgdsg,j',
    //   token: 'dhsgfhgdhjsfkgjhsd',
    //   tete: true,
    // });
    
    // firebase
    //   .firestore()
    //   .runTransaction(async transaction => {
    //     const doc = await transaction.get(ref);
    //     console.warn(doc);


    //     // if it does not exist set the population to one
    //     if (!doc.exists) {
    //       transaction.set(ref, { nome: '', token: '' });
    //       // return the new value so we know what the new population is
    //       return 1;
    //     }

    //     // exists already so lets increment it + 1
    //     const newPopulation = doc.data().population + 1;

    //     transaction.update(ref, {
    //       nome: 'éuuuuuu',
    //       token: 'Testes',
    //     });

    //     // return the new value so we know what the new population is
    //     return newPopulation;
    //   })
    //   .then(newPopulation => {
    //     console.warn(
    //       `Transaction successfully committed and new population is '${newPopulation}'.`
    //     );
    //   })
    //   .catch(error => {
    //     console.warn('Transaction failed: ', error);
    //   });
  }


  render() {
    return (
        <View style={styles.page}>
            <Appbar.Header> 
                <ToolbarAction icon="menu" onPress={() => Actions.drawerOpen()} />
                <Appbar.Content
                    title="Athena"
                    subtitle={null}
                />
            </Appbar.Header>
        
            <Searchbar
                placeholder="Search"
                onChangeText={query => { this.setState({ firstQuery: query }); }}
                value={this.state.firstQuery}
            />
         
            
        </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
});
