import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import { SocialIcon } from 'react-native-elements';

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from 'react-native-google-signin';

import {
  DefaultTheme,
  Appbar,
  TextInput,
  IconButton,
  Button,
  Card,
  Checkbox,
  Toolbar,
  ToolbarContent,
  Divider,
  Snackbar,
  Title,
 } from 'react-native-paper';

import {
  StyleSheet,
  Alert,
  Image,
  Text,
  View,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  ActivityIndicator,
  Platform,
  TouchableOpacity
} from 'react-native';

import { theme, sleep } from '../App.react';

import logo from '../Imagens/logo-athena-br-black.png';
// import { getTokenLogin } from '../fetchs/Autenticacao.react';
// import { getUsuarioLogado } from '../fetchs/Usuarios.react';

const { height, width } = Dimensions.get('window');;

const window = Dimensions.get('window');
export const IMAGE_HEIGHT = window.width / 2;
export const IMAGE_HEIGHT_SMALL = window.width /7;

type Props = {};
export default class Login extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      load: false,
      isLoad: false,
      isLoadSendMailResetSenha: false,
      visible: false,
      secureTextEntry: true,
      isSigninInProgress: false,
      lembrar: false,
      email: '',
      senha: '',
      user: firebase.auth().currentUser,
    };

    this.refUsuarios = firebase.firestore().collection('usuarios');
  }

  componentDidMount = async () => {
    await GoogleSignin.hasPlayServices();

    // setTimeout(async() => {
    //   if(this.state.user && this.state.user.emailVerified){
    //     const fcmToken = await firebase.messaging().getToken();
    //     const tabelaUsuario = await this.refUsuarios.where('token', '==', this.state.user.uid).get();
    //     console.warn(fcmToken);
    
    //     this.refUsuarios.doc(tabelaUsuario.docs[0].id).update({
    //       fcmToken,
    //     });

    //     Actions.grupos();
    //   } else if(this.state.user && !this.state.user.emailVerified){
    //     this.setState({ visible: true });
    //   }

    //   this.setState({ load: false });
    // }, 150);
  }

  onPressLogin = async () => {
    const { email, senha, lembrar } = this.state;

    if(!email || !senha){
      Alert.alert(
        'Ops!',
        'Preencha email e senha para sabermos quem é você!',
        [
          {text: 'OK', onPress: async () => {}},
        ],
        { cancelable: false }
      );
      return;
    }

    this.setState({ isLoad: true });

    const permitido = await this.entrar(email, senha);
    
    if(permitido) { // && permitido.user.emailVerified
      const usuario = await this.refUsuarios
        .where('token', '==', permitido.user.uid).get();

      this.refUsuarios.doc(usuario.docs[0].id).update({ lembrar });
  
      Actions.user = permitido.user;
      Actions.usuario = usuario.docs[0];
  
      Actions.grupos();       
    }           
    // } else if (permitido) { // && !permitido.user.emailVerified
    //   // permitido.user.sendEmailVerification();
    //   Alert.alert(
    //     'Ops!',
    //     'Voce ainda não verificou seu e-mail? Poxa vai lá, é para a sua segunraça!',
    //     [
    //       {text: 'OK', onPress: () => console.log('OK Pressed')},
    //     ],
    //     { cancelable: false }
    //   );
    // }

    this.setState({ isLoad: false });
  }

  getMensagemErroEntrar = (code) => {
    switch (code) {
      case 'auth/invalid-email':
        return 'Email invalido!';
        break;
      case 'auth/user-disabled':
        return 'Usuario foi desativado!';
        break;
      case 'auth/user-not-found':
        return 'Usuario não encontrado!';
        break;
      case 'auth/wrong-password':
        return 'Senha invalida !';
        break;
      
      default:
        return 'Não foi possivel identificar o que aconteceu!\nTente novamente em alguns instantes!';
        break;
    }
  }

  entrar = async (usuario, senha) => {
    try {
      const user = await firebase.auth().signInWithEmailAndPassword(usuario, senha);
      Actions.user = user;
      return user;
      
    } catch (error) {
      const msg = this.getMensagemErroEntrar(error.code);

      Alert.alert(
        'Ops!',
        msg,
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
      return false;
    }
  }

  getMensagemErroCadastro = (code) => {
    switch (code) {
      case 'auth/invalid-email':
        return 'Email invalido!';
        break;
      case 'auth/email-already-in-use':
        return 'Já existe uma conta com este e-mail.';
        break;
      case 'auth/operation-not-allowed':
        return 'Usuario desativado.';
        break;
      case 'auth/weak-password':
        return 'Sua senha não é forte o suficiente!';
        break;
      
      default:
        return 'Não foi possivel identificar o que aconteceu!\nTente novamente em alguns instantes!';
        break;
    }
  }

  criarUsuario = async ({ id, email, photo, name, accessToken  }) => {
    try {
      const cadastro = await firebase.auth().createUserWithEmailAndPassword(email, id);
      cadastro.user.updateProfile({ displayName: name, photoURL: photo });

      this.refUsuarios.add({
        photoURL: photo,
        nome: name,
        token: cadastro.user.uid,
        amigos: [],
        tokenGoogle: id,
      });

      cadastro.user.sendEmailVerification();
    } catch (error) {
      const msg = this.getMensagemErroCadastro(error.code);
      if(!msg == 'Já existe uma conta com este e-mail.'){
        Alert.alert(
          'Ops!',
          msg,
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
        return false;
      }
    }

    this.setState({ email, senha: id, accessToken: id, lembrar: true });
    await sleep(250);
    this.onPressLogin();
    await sleep(450);
    this.setState({ email: '', senha: '', accessToken: '', lembrar: false });
  }

  signIn = async () => {
      // const usr = await GoogleSignin.currentUserAsync();
      await GoogleSignin.signIn()
          .then((resp) => {
              this.setState({ isSigninInProgress: true });
              this.criarUsuario(resp.user);
          })
          .catch((err) => {
              console.warn('WRONG SIGNIN', err.message);
          })
          .done();

    this.setState({ isSigninInProgress: false });
  };

  renderAppBar = () => (
    <Appbar.Header>
      <Appbar.Content
          title="Minha conta"
          subtitle={null}
      />
    </Appbar.Header>
  )

  renderFormulario = () => (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 0}
      flex={1}
    >
      <TextInput
        label='E-mail'
        style={[styles.input, { flex: 1 }]}
        keyboardType="email-address"
        value={this.state.email}
        onChangeText={email => this.setState({ email })}
      />

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <TextInput
          secureTextEntry={this.state.secureTextEntry}
          label='Senha'
          style={{ flex: 1 }}
          value={this.state.senha}
          onChangeText={senha => this.setState({ senha })}
        />

        <IconButton
          icon={this.state.secureTextEntry ? 'visibility-off' : 'visibility'}
          color={this.state.secureTextEntry ? theme.colors.disabled : theme.colors.primary}
          size={28}
          onPress={() => this.setState({ secureTextEntry: !this.state.secureTextEntry })}
        />

        {/* <Checkbox
          status={this.state.secureTextEntry ? 'unchecked' : 'checked'}
          onPress={() => { this.setState({ secureTextEntry: !this.state.secureTextEntry }); }}
        /> */}

      </View>

      <TouchableOpacity
        style={{  alignItems: 'center', flexDirection: 'row', marginBottom: 15 }}
        onPress={() => { this.setState({ lembrar: !this.state.lembrar }); }}
      >
          <Checkbox
            status={this.state.lembrar ? 'checked' : 'unchecked'}
          />
          <Text>Lembrar e-mail e senha</Text>
      </TouchableOpacity>

      <Button
        primary
        mode="contained"
        loading={this.state.isLoad}
        disabled={this.state.isLoad}
        style={styles.botaoLogar}
        onPress={this.onPressLogin}
      >
        <Text style={{ fontSize: 18 }}>ENTRAR</Text>
      </Button>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Button
          primary
          loading={this.state.isLoadSendMailResetSenha}
          style={[styles.botaoLogar, { marginTop: 10 }]}
          onPress={async () => {
            if(!this.state.email) {
              Alert.alert(
                'Ops!',
                'Digite seu e-mail!',
                [
                  {text: 'OK', onPress: async () => {}},
                ],
                { cancelable: false }
              );
              return;
            }

            this.setState({ isLoadSendMailResetSenha: true });
            try {
              const mail = await firebase.auth().sendPasswordResetEmail(this.state.email);
            
              Alert.alert(
                'Pronto!',
                'Você deve receber em instantes um e-mail para trocar sua senha!',
                [
                  {text: 'OK', onPress: async () => {}},
                ],
                { cancelable: false }
              );
              
            } catch (error) {
              Alert.alert(
                'Ops!',
                'E-mail não encontrado ou houve algo de errado!',
                [
                  {text: 'OK', onPress: async () => {}},
                ],
                { cancelable: false }
              );
            }      
            this.setState({ isLoadSendMailResetSenha: false });
          }}
        >
          Esqueci a senha
        </Button>

        <Button
          primary
          style={[styles.botaoLogar, { marginTop: 10 }]}
          onPress={() => Actions.cadastro()}
        >
          Nova conta
        </Button>
      
      </View>

      <Divider />

      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Title>Ou</Title>
      </View>

      <GoogleSigninButton
        style={{ height: 48 }}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Light}
        onPress={this.signIn}
        disabled={this.state.isSigninInProgress}
      />


    </KeyboardAvoidingView>
  )

  renderContainer = () => (
    <View style={{ marginHorizontal: 10 }}>
      <Image source={logo} style={styles.logo}/>
      <Card flex={1}>
        <Card.Content>
          {this.renderFormulario()}
        </Card.Content>
      </Card>
    </View>
  )

  renderLoading = () => (
    <View style={[styles.container, { backgroundColor: 'transparent' }]}>
        <ActivityIndicator size="large" color={theme.colors.primary} />
        <Text>Aguarde...</Text>
    </View>
  )

  render = () => (
    <View flex={1} backgroundColor="#F5F5F5">
      {this.renderAppBar()}
      {
        this.state.load
        ? this.renderLoading()
        :
        <ScrollView>
          {this.renderContainer()}
        </ScrollView>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width,
    height: 150,
    resizeMode: 'contain',
  },
  input: {
    marginBottom: 10,
  },
  containerInputs: {
    marginVertical: 5,
    borderRadius: 5,
  },
  botaoLogar: {
  },
  buttonLogarComFacebook: {
    height: 52,
    borderRadius: 2,
    paddingHorizontal: 10,
  },
  buttonLogarComGoogle: {
      height: 52,
      borderRadius: 2,
      paddingHorizontal: 10,
  },
});
